$(document).ready(function () {

    // Animation
    AOS.init();

    // Wow Animation
    if ($(".animated").length) {
        wow = new WOW({
            boxClass: "wow", // default
            animateClass: "animated", // default
            offset: 0, // default
            mobile: true, // default
            live: true, // default
        });
        wow.init();
    }

    // video popup
    $(".video-link").magnificPopup({
        type: "iframe",
        // other options
    });

    // content popup
    $(".popup-link").magnificPopup({
        type: "inline",
        // other options
    });

    $('.owl-slider').owlCarousel({
        items: 2,
        loop: false,
        margin: 20,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 2,
                nav: true
            },
            600: {
                items: 3,
                nav: true,
            },
            1000: {
                items: 5,
                nav: true,
            },
        }
    });

    $(".popup-image").magnificPopup({
        type: "image",
        closeOnContentClick: true,
        closeBtnInside: true,
        fixedContentPos: true,
        mainClass: "mfp-no-margins mfp-with-zoom", // class to remove default margin from left and right side
        image: {
            verticalFit: true,
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
        },
    });

    /* Form Validation */
    // $("form").each(function () {
    //     var form = $(this);
    //     if (form.length) {
    //         form.validate({
    //             rules: {
    //                 phone: {
    //                     required: true,
    //                     number: true,
    //                 },
    //                 email: {
    //                     required: true,
    //                     email: true,
    //                 },
    //             },
    //             submitHandler: function (form) {
    //                 console.log(form.id);
    //                 var btn = $("#" + form.id + ' [type="submit"]'),
    //                     _form = $(form),
    //                     loading = _form.find(".loading");
    //                 loading.fadeIn(), btn.attr("disabled", ""), _form.addClass("disabled");

    //                 $("input[name='phone']").removeAttr("disabled");

    //                 $.ajax({
    //                     url: "saveInfo.php",
    //                     type: form.method,
    //                     data: $(form).serialize(),
    //                     success: function (data) {
    //                         loading.fadeOut(), btn.removeAttr("disabled"), _form.removeClass("disabled");
    //                         if (data) {
    //                             try {
    //                                 data = jQuery.parseJSON(data);
    //                             } catch (e) {
    //                                 console.log(e); // error in the above string (in this case, yes)!
    //                             }
    //                         }
    //                         response = data.response;
    //                         console.log(data);
    //                         if (response.code == 0) {
    //                             ohSnap('Failed sending your informations, please try again!', { color: 'red' });
    //                         }
    //                         else if (response.code == 4) {
    //                             ohSnap('Invalid Project Selected!', { color: 'red' });
    //                         }
    //                         else if (response.code == 3) {
    //                             form.reset();

    //                             ohSnap('Your information successfully reached us.', { color: 'green', 'duration': '3000' });

    //                             setTimeout(function () {
    //                                 window.location = "thankyou.php";
    //                             }, 3000);
    //                         } else if (response.code == 2) {
    //                             ohSnap('User already exists!', { color: 'green', 'duration': '3000' });
    //                         } else {
    //                             ohSnap('Technical Error: Please contact administrator!', { color: 'green' });
    //                         }
    //                     },
    //                 });
    //                 return false;
    //             },
    //         });
    //     }
    // });

    $(".stickyForm .btn, .headingTitle button.btn.btn-primary").on("click", function () {
        $(".stickyForm").toggleClass("active");
    });

    $(".key-procedures-tabs button").on("click", function () {
        tab = $(this).data("tab");
        $(".key-procedures-tabs button").removeClass("active");
        $(this).addClass("active");
        $(".key-procedures-content .kp-content-box").removeClass("active fadeIn animated");
        $(".key-procedures-content ." + tab + "").addClass("active fadeIn animated");
    });

    //anchor scroll smooth
    /*$('a[href*="#"]:not([href="#"])')
        .not(".tabs a, ._tabs a, #accordion .panel-title > a")
        .click(function () {
            if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
                var target = $(this.hash);
                // target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
                if (target.length) {
                    $("html, body").animate(
                        {
                            scrollTop: target.offset().top-90,
                        },
                        100
                    );
                    return false;
                }
            }
        });*/


    /**/
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 100) {
            $('header').addClass('fixed-header wow animated slideInDown');
        }
        else {
            $('header').removeClass('fixed-header wow animated slideInDown');
        }
    });

    window_width = window.innerWidth;

    $(window).scroll(function () {

        var scroll = $(window).scrollTop();

        if (scroll > 100) {
            $(".scrollTop").addClass("up");
        } else {
            $(".scrollTop").removeClass("up");
        }

    });

    $("body").on("click", ".scrollTop", function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });


    /*Tabs*/
    $(".tabsLists ul li").on("click", function () {

        $(".tabsContent").removeClass("active animated wow slideInUp");
        $(".tabsLists ul li").removeClass("active");

        $(this).addClass("active");

        var target = $(this).data('target');

        $(target).addClass("active animated wow slideInUp");

    })
    /*Tabs*/

    $(".phone").intlTelInput({
        defaultCountry: "auto",
        initialCountry: "auto",
        geoIpLookup: function (callback) {
            callback('IN')
        },
        utilsScript: "./js/utils.js"
    }),
        $(".phone").on("countrychange", function (e, countryData) {
            var dialCode = countryData.dialCode
                , countryCodeElem = $('#country_code, #country_code_popup')
            countryCodeElem.val(dialCode)
        });

    $(".digit-group")
        .find("input")
        .each(function () {
            $(this).attr("maxlength", 1);
            $(this).on("keyup", function (e) {
                var parent = $($(this).parent());
                if (e.keyCode === 8 || e.keyCode === 37) {
                    var prev = parent.find("input#" + $(this).data("previous"));
                    if (prev.length) {
                        $(prev).select();
                    }
                } else if ((e.keyCode >= 48 && e.keyCode <= 57) ||
                    (e.keyCode >= 65 && e.keyCode <= 90) ||
                    (e.keyCode >= 96 && e.keyCode <= 105) ||
                    e.keyCode === 39
                ) {
                    var next = parent.find("input#" + $(this).data("next"));
                    if (next.length) {
                        $(next).select();
                    }
                }
            });
        });

    $("#phone, #phone_popup").on("blur", function () {

        if ($(this).val().length > 3) {

            // $("input[name='phone']").attr('disabled', 'disabled');

            $(".phoneEdit").show();

            $.ajax({
                type: 'POST',
                url: 'saveInfo.php',
                data: {
                    name: $("input[name = 'name']").val(),
                    phone: $("input[name = 'phone']").val(),
                },
                dataType: 'json',
                success: function (data) {

                    if (data) {
                        try {
                            data = jQuery.parseJSON(data);
                        } catch (e) {
                            console.log(e); // error in the above string (in this case, yes)!
                        }
                    }
                    response = data.response;

                    if (response.code == 1) {

                        $("#otp, #otp_popup").slideDown();
                        $("#otp input[name='otp1'], #otp_popup input[name='otp1']").select();

                        $("#formMessage").text("OTP has been sent successfully");
                        $("#formMessage").addClass("success");
                        $("#formMessage").addClass("active");

                        setTimeout(function () {
                            $("#formMessage").removeClass("active");
                            $("#formMessage").removeClass("success");
                            $("#formMessage").text("");
                        }, 3000);


                    } else {

                        $("#formMessage").text("Something went wrong,please try later");
                        $("#formMessage").addClass("error");
                        $("#formMessage").addClass("active");

                        setTimeout(function () {
                            $("#formMessage").removeClass("active");
                            $("#formMessage").removeClass("error");
                            $("#formMessage").text("");
                        }, 5000);


                    }

                }
            });

        }

    });

    $("#digit-5").on("blur", function () {
        if ($("#digit-1").val() != '' &&
            $("#digit-2").val() != '' &&
            $("#digit-3").val() != '' &&
            $("#digit-4").val() != '' &&
            $("#digit-5").val() != ''
        ) {

            let otp = $("input[name = 'otp1']").val() +
                $("input[name = 'otp2']").val() +
                $("input[name = 'otp3']").val() +
                $("input[name = 'otp4']").val() +
                $("input[name = 'otp5']").val();

            $.ajax({
                type: 'POST',
                url: 'saveInfo.php',
                data: {
                    phone: $("input[name = 'phone']").val(),
                    otp: otp,
                },
                dataType: 'json',
                success: function (data) {

                    if (data) {
                        try {
                            data = jQuery.parseJSON(data);
                        } catch (e) {
                            console.log(e); // error in the above string (in this case, yes)!
                        }
                    }
                    response = data.response;

                    if (response.code == 2) {

                        $("div#otp, div#otp_popup").hide();

                        $("input[name='email'] ").select();

                        $("#formMessage").text("Your mobile number is verified");
                        $("#formMessage").addClass("success");
                        $("#formMessage").addClass("active");

                        setTimeout(function () {
                            $("#formMessage").removeClass("active");
                            $("#formMessage").removeClass("success");
                            $("#formMessage").text("");
                        }, 3000);


                    } else {

                        $("#formMessage").text("Invalid OTP, please try again later");
                        $("#formMessage").addClass("error");
                        $("#formMessage").addClass("active");

                        setTimeout(function () {
                            $("#formMessage").removeClass("active");
                            $("#formMessage").removeClass("error");
                            $("#formMessage").text("");
                        }, 5000);


                    }

                }
            });

        }
    });


    $("#digit-5_popup").on("blur", function () {
        if ($("#digit-1_popup").val() != '' &&
            $("#digit-2_popup").val() != '' &&
            $("#digit-3_popup").val() != '' &&
            $("#digit-4_popup").val() != '' &&
            $("#digit-5_popup").val() != ''
        ) {

            let otp = $("input[name = 'otp1']").val() +
                $("input[name = 'otp2']").val() +
                $("input[name = 'otp3']").val() +
                $("input[name = 'otp4']").val() +
                $("input[name = 'otp5']").val();

            $.ajax({
                type: 'POST',
                url: 'saveInfo.php',
                data: {
                    phone: $("input[name = 'phone']").val(),
                    otp: otp,
                },
                dataType: 'json',
                success: function (data) {

                    if (data) {
                        try {
                            data = jQuery.parseJSON(data);
                        } catch (e) {
                            console.log(e); // error in the above string (in this case, yes)!
                        }
                    }
                    response = data.response;

                    if (response.code == 2) {

                        $("div#otp, div#otp_popup").hide();

                        $("input[name='email'] ").select();

                        $("#formMessage").text("Your mobile number is verified");
                        $("#formMessage").addClass("success");
                        $("#formMessage").addClass("active");

                        setTimeout(function () {
                            $("#formMessage").removeClass("active");
                            $("#formMessage").removeClass("success");
                            $("#formMessage").text("");
                        }, 3000);


                    } else {

                        $("#formMessage").text("Invalid OTP, please try again later");
                        $("#formMessage").addClass("error");
                        $("#formMessage").addClass("active");

                        setTimeout(function () {
                            $("#formMessage").removeClass("active");
                            $("#formMessage").removeClass("error");
                            $("#formMessage").text("");
                        }, 5000);


                    }

                }
            });

        }
    });

    $("body").on("click", ".phoneEdit", function () {
        $(this).parents(".box").find("input[name='phone']").removeAttr("disabled");
        $(this).hide();
        $(this).parents(".box").find("input[name='phone']").select();
    })

    $(".form-check-input[type=checkbox]").on("change", function () {
        if ($(this).prop('checked') == false) {
            alert("Please contact administrator!");
            $(this).prop('checked', true);
        }
    });

});

document.addEventListener("DOMContentLoaded", function (event) {
    var iframeData = $("#map iframe");
    var iframeDataSrc = iframeData.data('src');
    iframeData.attr('src', iframeDataSrc);
});



