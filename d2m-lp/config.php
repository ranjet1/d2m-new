<?php 

/**
 * Constant Variables
 */

error_reporting(E_ALL);
ini_set('display_errors', 'On');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Name */
define('DB_NAME', 'd2m'); // Insert DB name here

/** Database User */
define('DB_USER', 'root'); // Insert DB username here

/** Database User */
define('DB_PASS', ''); // Insert DB password here

define('_TABLE_NAME_', 'd2m_lp_leads'); // Insert table name here


/** Site Base URL */
// define('BASE_URL', '');

define('LP_NAME', 'D2M Landing Page');

define('EMAIL_FROM_NAME', '');
define('FROM_EMAIL', '');


DEFINE ('CONTACT_TO',''); //
  
 //MULTIPLE EMAIL COMMA SEPERATED  
DEFINE ('CONTACT_CC','');         //MULTIPLE EMAIL COMMA SEPERATED  
DEFINE ('CONTACT_BCC','');       //MULTIPLE EMAIL COMMA SEPERATED 
DEFINE ('CONTACT_SUBJECT', LP_NAME.' Enquiry');

 
global $conn;

$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);

if (!$conn)
{
	 die('Could not connect: ' . mysqli_error());
}


// session start
set_time_limit(0);
session_start();