<?php

if (isset($_GET['utm_source']) && $_GET['utm_source'] != '') {

    $utm_source = $_GET['utm_source'];
} else {

    $utm_source = 'Digital Marketing';
}

if (isset($_GET['secondarysource']) && $_GET['secondarysource'] != '') {

    $secondarySource = $_GET['secondarysource'];
} else {

    $secondarySource = 'Website';
}

if (isset($_GET['tertiarysource']) &&  $_GET['tertiarysource'] != '') {

    $tertiarySource = $_GET['tertiarysource'];
} else {

    $tertiarySource = 'Microsite';
}



$utm_medium = (isset($_GET['utm_medium']) ? $_GET['utm_medium'] : '');
$utm_campaign = (isset($_GET['utm_campaign']) ? $_GET['utm_campaign'] : '');
$utm_matchtype = (isset($_GET['matchtype']) ? $_GET['matchtype'] : '');
$utm_keyword = (isset($_GET['keyword']) ? $_GET['keyword'] : '');
$utm_device = (isset($_GET['device']) ? $_GET['device'] : '');
$utm_placement = (isset($_GET['placement']) ? $_GET['placement'] : '');
$utm_term = (isset($_GET['utm_term']) ? $_GET['utm_term'] : '');

// New Parameters
$gclid = (isset($_GET['gclid']) ? $_GET['gclid'] : '');
$utm_term = (isset($_GET['utm_term']) ? $_GET['utm_term'] : '');
$utm_content = (isset($_GET['utm_content']) ? $_GET['utm_content'] : '');


// $original_referrer = $_SERVER['HTTP_REFERER'];

$original_referrer = "";

if (!$original_referrer) {
    $original_referrer = "Direct";
} else {
    $parse = parse_url($original_referrer);
    $original_referrer = $parse['host'];
}

$utmctr = (isset($_GET['utmctr']) ? $_GET['utmctr'] : '');
// New Parameters

$pageUrl = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

$user_agent = $_SERVER['HTTP_USER_AGENT'];
$devices = ["lighthouse", "light house", "google-amp", "google amp", "page-speed", "page speed"];
$crawlagent = false;
if ($user_agent)
    foreach ($devices as $device) {
        $isMatch = (bool) preg_match("/" . $device . "/i", $user_agent);
        if ($isMatch) {
            $crawlagent =  $device;
            break;
        }
    }
if (isset($_GET['debug']))
    $crawlagent = 'agent';
?>
<html lang="">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title>D2m</title>

    <meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />
    <link rel="canonical" href="https://shriramsolitaire.com/" />
    <meta name="facebook-domain-verification" content="0dsz55h5zil01uktye54eo0rkd5yg1" />

    <meta name="description" content="Shriram Solitaire offers premium 2 &3 BHK homes that offer panoramic views of the Veerasagara Lake and lush greenery with 30+ modern amenities. Click on the link, now" />

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <link rel="shortcut icon" href="assets/images/logo.png" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/logo.png" />

    <?php if (!$crawlagent) : ?>
        <script type="text/javascript">
            let agg = navigator.userAgent;
            var __ac_ = agg.includes("Lighthouse");
        </script>
        <script type="text/javascript">
            scripts_loaded = false;
            class RocketLazyLoadScripts {
                constructor(e) {
                    (this.triggerEvents = e), (this.eventOptions = {
                        passive: !0
                    }), (this.userEventListener = this.triggerListener.bind(this)), (this.delayedScripts = {
                        normal: [],
                        async: [],
                        defer: []
                    }), (this.allJQueries = []);
                }
                _addUserInteractionListener(e) {
                    this.triggerEvents.forEach((t) => window.addEventListener(t, e.userEventListener, e.eventOptions));
                }
                _removeUserInteractionListener(e) {
                    this.triggerEvents.forEach((t) => window.removeEventListener(t, e.userEventListener, e.eventOptions));
                }
                triggerListener() {
                    this._removeUserInteractionListener(this), "loading" === document.readyState ? document.addEventListener("DOMContentLoaded", this._loadEverythingNow.bind(this)) : this._loadEverythingNow();
                }
                async _loadEverythingNow() {
                    if (!scripts_loaded) {
                        scripts_loaded = true;
                        console.log("loading now");
                        this._delayEventListeners(),
                            this._delayJQueryReady(this),
                            this._handleDocumentWrite(),
                            this._registerAllDelayedScripts(),
                            this._preloadAllScripts(),
                            await this._loadScriptsFromList(this.delayedScripts.normal),
                            await this._loadScriptsFromList(this.delayedScripts.defer),
                            await this._loadScriptsFromList(this.delayedScripts.async),
                            await this._triggerDOMContentLoaded(),
                            await this._triggerWindowLoad(),
                            window.dispatchEvent(new Event("rocket-allScriptsLoaded"));
                    }
                }
                _registerAllDelayedScripts() {
                    document.querySelectorAll("script[type=rocketlazyloadscript]").forEach((e) => {
                        e.hasAttribute("src") ?
                            e.hasAttribute("async") && !1 !== e.async ?
                            this.delayedScripts.async.push(e) :
                            (e.hasAttribute("defer") && !1 !== e.defer) || "module" === e.getAttribute("data-rocket-type") ?
                            this.delayedScripts.defer.push(e) :
                            this.delayedScripts.normal.push(e) :
                            this.delayedScripts.normal.push(e);
                    });
                }
                async _transformScript(e) {
                    return (
                        await this._requestAnimFrame(),
                        new Promise((t) => {
                            const n = document.createElement("script");
                            let i;
                            [...e.attributes].forEach((e) => {
                                    let t = e.nodeName;
                                    "type" !== t && ("data-rocket-type" === t && ((t = "type"), (i = e.nodeValue)), n.setAttribute(t, e.nodeValue));
                                }),
                                e.hasAttribute("src") ? (n.addEventListener("load", t), n.addEventListener("error", t)) : ((n.text = e.text), t()),
                                e.parentNode.replaceChild(n, e);
                        })
                    );
                }
                async _loadScriptsFromList(e) {
                    const t = e.shift();
                    return t ? (await this._transformScript(t), this._loadScriptsFromList(e)) : Promise.resolve();
                }
                _preloadAllScripts() {
                    var e = document.createDocumentFragment();
                    [...this.delayedScripts.normal, ...this.delayedScripts.defer, ...this.delayedScripts.async].forEach((t) => {
                            const n = t.getAttribute("src");
                            if (n) {
                                const t = document.createElement("link");
                                (t.href = n), (t.rel = "preload"), (t.as = "script"), e.appendChild(t);
                            }
                        }),
                        document.head.appendChild(e);
                }
                _delayEventListeners() {
                    let e = {};

                    function t(t, n) {
                        !(function(t) {
                            function n(n) {
                                return e[t].eventsToRewrite.indexOf(n) >= 0 ? "rocket-" + n : n;
                            }
                            e[t] ||
                                ((e[t] = {
                                        originalFunctions: {
                                            add: t.addEventListener,
                                            remove: t.removeEventListener
                                        },
                                        eventsToRewrite: []
                                    }),
                                    (t.addEventListener = function() {
                                        (arguments[0] = n(arguments[0])), e[t].originalFunctions.add.apply(t, arguments);
                                    }),
                                    (t.removeEventListener = function() {
                                        (arguments[0] = n(arguments[0])), e[t].originalFunctions.remove.apply(t, arguments);
                                    }));
                        })(t),
                        e[t].eventsToRewrite.push(n);
                    }

                    function n(e, t) {
                        let n = e[t];
                        Object.defineProperty(e, t, {
                            get: () => n || function() {},
                            set(i) {
                                e["rocket" + t] = n = i;
                            },
                        });
                    }
                    t(document, "DOMContentLoaded"), t(window, "DOMContentLoaded"), t(window, "load"), t(window, "pageshow"), t(document, "readystatechange"), n(document, "onreadystatechange"), n(window, "onload"), n(window, "onpageshow");
                }
                _delayJQueryReady(e) {
                    let t = window.jQuery;
                    Object.defineProperty(window, "jQuery", {
                        get: () => t,
                        set(n) {
                            if (n && n.fn && !e.allJQueries.includes(n)) {
                                n.fn.ready = n.fn.init.prototype.ready = function(t) {
                                    e.domReadyFired ? t.bind(document)(n) : document.addEventListener("rocket-DOMContentLoaded", () => t.bind(document)(n));
                                };
                                const t = n.fn.on;
                                (n.fn.on = n.fn.init.prototype.on = function() {
                                    if (this[0] === window) {
                                        function e(e) {
                                            return e
                                                .split(" ")
                                                .map((e) => ("load" === e || 0 === e.indexOf("load.") ? "rocket-jquery-load" : e))
                                                .join(" ");
                                        }
                                        "string" == typeof arguments[0] || arguments[0] instanceof String ?
                                            (arguments[0] = e(arguments[0])) :
                                            "object" == typeof arguments[0] &&
                                            Object.keys(arguments[0]).forEach((t) => {
                                                delete Object.assign(arguments[0], {
                                                    [e(t)]: arguments[0][t]
                                                })[t];
                                            });
                                    }
                                    return t.apply(this, arguments), this;
                                }),
                                e.allJQueries.push(n);
                            }
                            t = n;
                        },
                    });
                }
                async _triggerDOMContentLoaded() {
                    (this.domReadyFired = !0),
                    await this._requestAnimFrame(),
                        document.dispatchEvent(new Event("rocket-DOMContentLoaded")),
                        await this._requestAnimFrame(),
                        window.dispatchEvent(new Event("rocket-DOMContentLoaded")),
                        await this._requestAnimFrame(),
                        document.dispatchEvent(new Event("rocket-readystatechange")),
                        await this._requestAnimFrame(),
                        document.rocketonreadystatechange && document.rocketonreadystatechange();
                }
                async _triggerWindowLoad() {
                    await this._requestAnimFrame(),
                        window.dispatchEvent(new Event("rocket-load")),
                        await this._requestAnimFrame(),
                        window.rocketonload && window.rocketonload(),
                        await this._requestAnimFrame(),
                        this.allJQueries.forEach((e) => e(window).trigger("rocket-jquery-load")),
                        window.dispatchEvent(new Event("rocket-pageshow")),
                        await this._requestAnimFrame(),
                        window.rocketonpageshow && window.rocketonpageshow();
                }
                _handleDocumentWrite() {
                    const e = new Map();
                    document.write = document.writeln = function(t) {
                        const n = document.currentScript,
                            i = document.createRange(),
                            r = n.parentElement;
                        let o = e.get(n);
                        void 0 === o && ((o = n.nextSibling), e.set(n, o));
                        const a = document.createDocumentFragment();
                        i.setStart(a, 0), a.appendChild(i.createContextualFragment(t)), r.insertBefore(a, o);
                    };
                }
                async _requestAnimFrame() {
                    return new Promise((e) => requestAnimationFrame(e));
                }
                static run(forceLoad) {
                    const e = new RocketLazyLoadScripts(["keydown", "mousemove", "touchmove", "touchstart", "touchend", "wheel"]);
                    if (forceLoad == "force") {
                        e._loadEverythingNow();
                        return;
                    }
                    e._addUserInteractionListener(e);
                }
            }
        </script>
        <script type="text/javascript">
            window.onload = function() {
                if (!__ac_) {
                    RocketLazyLoadScripts.run();
                }
                if (!__ac_) {
                    /* optimisation code 1*/

                    lzyCSS("assets/css/animate.css");
                    lzyCSS("assets/css/aos.css");
                    lzyCSS("assets/css/owl.carousel.min.css");
                    lzyCSS("assets/css/magnific-popup.css");
                    lzyCSS("assets/css/ohsnap.css");
                    lzyCSS("assets/css/intlTelInput.css");

                    setTimeout(function() {
                        if (!scripts_loaded) {
                            RocketLazyLoadScripts.run("force");
                        }
                    }, 500);
                }
            };
        </script>
    <?php endif ?>
    <!-- optimisation code 3 -->
    <script type="text/javascript">
        var lzyCSS = function(href) {
            var l = document.createElement("link");
            l.rel = "stylesheet";
            l.href = href;
            var h = document.getElementsByTagName("head")[0];
            h.appendChild(l);
        };
    </script>

    <!-- Stylesheets
    ================================================== -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/fonts/font.css?php echo rand(100, 1000); ?>" />
    <link rel="stylesheet" href="assets/css/style.css?<?php echo rand(100, 1000); ?>" />
    <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

    <!-- Stylesheets
    ================================================== -->

    <!-- Fonts
    ================================================== -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" />
    <!-- Fonts
    ================================================== -->

</head>

<body class="page">

    <div class="content">
        <header id="header" class="absolute_header">
            <div class="container">

                <div class="row align-items-center justify-content-center">
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <div class="brand-logo">
                            <img src="assets/images/logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                        <ul class="menu">
                            <li>
                                <a href="">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#contactUs">
                                    Contact Us
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </header>
        <div class="heroBanner">
            <div class="container">

                <div class="headingTitle">
                    <h1> <span class="green_text">Find your</span> <span class="white_text">Perfect Car</span> </h1>
                    <button class="btn btn-primary">RESERVE NOW</button>
                </div>

            </div>
        </div>
        <section class="pink_bg space-pd overview_section">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="titleBox bg_G">
                            <h4>Driving a car just got easier.</h4>
                            <h2>Have a <span class="green_text">Great Experience</span></h2>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                        <div class="boxWrapper">
                            <div class="flex">

                                <div class="iconHead">
                                    <img src="assets/images/icons-1/1.svg" alt="">
                                </div>
                                <div class="caption">
                                    Hassle-free booking experience
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                        <div class="boxWrapper">
                            <div class="flex">

                                <div class="iconHead">
                                    <img src="assets/images/icons-1/2.png" alt="">
                                </div>
                                <div class="caption">
                                    Hassle-free booking experience
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                        <div class="boxWrapper">
                            <div class="flex">

                                <div class="iconHead">
                                    <img src="assets/images/icons-1/3.svg" alt="">
                                </div>
                                <div class="caption">
                                    Hassle-free booking experience
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
                        <div class="boxWrapper">
                            <div class="flex">

                                <div class="iconHead">
                                    <img src="assets/images/icons-1/4.png" alt="">
                                </div>
                                <div class="caption">
                                    Hassle-free booking experience
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="car_overviewMatch space-pd">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <div class="image_box">
                            <img src="assets/images/why_d2m_image.webp" alt="">
                        </div>
                    </div>
                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6">
                        <div class="titleBox">
                            <img src="assets/images/why-title.png" alt="">
                        </div>
                        <div class="content">
                            <p class="lead">
                                If you are a company looking for hassle-free Car Driving Experience that allows you to USE capital in a more efficient way D2Mech is for you.
                            </p>
                            <p class="lead">
                                You can get the experience of driving a New Car of your choice without making a down payment. Also, our Long Term CarRental service helps you save taxes and forget about loans. We have a range of car models to choose from. Pick a car that will enhance your driving experience.
                            </p>
                            <ul>
                                <li><i class="fa-solid fa-circle-check"></i> Lower monthly costs </li>
                                <li><i class="fa-solid fa-circle-check"></i> 100% self-driven cars </li>
                                <li><i class="fa-solid fa-circle-check"></i> Complete transparency</li>
                            </ul>
                            <div class="signature_image">
                                <img src="assets/images/signatur.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="space-pd light_green_bg drive_easier_wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="titleBox bg_G">
                            <img src="assets/images/your-drive.png" alt="">
                            <p class="sub">
                                EXPERIENCE A HASSLE-FREE DRIVE
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3">
                        <div class="green_box">
                            <div class="icon_image">
                                <img src="assets/images/icons-2/1.png" alt="">
                            </div>
                            <div class="caption">
                                By removing all the things that increase the price of traditional car ownership, we provide you the easiest and most affordable way to get a car.
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3">
                        <div class="green_box">
                            <div class="icon_image">
                                <img src="assets/images/icons-2/2.png" alt="">
                            </div>
                            <div class="caption">Zero Down - Affordable Payment - Zero Hidden Fees.</div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3">
                        <div class="green_box">
                            <div class="icon_image">
                                <img src="assets/images/icons-2/3.png" alt="">
                            </div>
                            <div class="caption">ReFresh Your Car every 3 Years.</div>
                        </div>
                    </div>
                    <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3">
                        <div class="green_box">
                            <div class="icon_image">
                                <img src="assets/images/icons-2/4.png" alt="">
                            </div>
                            <div class="caption">All Coverage - Insurance, Maintenance and RoadSide Assistance all included in the Monthly Fee.</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="footerForm" style="background-color: #42AD4F;" id="contactUs">
            <div class="container">
                <form action="saveInfo.php" method="post" class="footer_form" id="footer_form">
                    <div class="titleBox">
                        <h2>Contact Us</h2>
                    </div>
                    <div class="footerFormInner">
                        <div class="input-block">
                            <div class="label">Full Name</div>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="input-block">
                            <div class="label">Email Id</div>
                            <input type="email" class="form-control" name="email" required>
                        </div>
                        <div class="input-block">
                            <div class="label">Phone Number</div>
                            <input type="hidden" name="country_code" id="country_code_footer_form" value="91" />
                            <input type="text" name="phone" id="phone" minlength="5" maxlength="15" required="" class="phone form-control" />
                        </div>
                        <div class="input-block">
                            <div class="label"></div>
                            <input type="submit" class="form-control" name="submit" value="SUBMIT">
                        </div>
                    </div>
                    <!-- UTM Data -->
                    <input type="hidden" name="form_name" value="Footer Form" />
                    <input type="hidden" name="project" value="D2M Landing Page" />
                    <input type="hidden" name="utm_source" value="<?php echo $utm_source; ?>" />
                    <input type="hidden" name="utm_medium" value="<?php echo $utm_medium; ?>" />
                    <input type="hidden" name="utm_campaign" value="<?php echo $utm_campaign; ?>" />
                    <input type="hidden" name="matchtype" value="<?php echo $utm_matchtype; ?>" />
                    <input type="hidden" name="keyword" value="<?php echo $utm_keyword; ?>" />
                    <input type="hidden" name="device" value="<?php echo $utm_device; ?>" />
                    <input type="hidden" name="placement" value="<?php echo $utm_placement; ?>" />
                    <input type="hidden" name="pageUrl" value='<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>' />
                    <input type="hidden" name="gclid" value="<?php echo $gclid; ?>" />
                    <input type="hidden" name="original_referrer" value="<?php echo $original_referrer; ?>" />
                    <input type="hidden" name="utmctr" value="<?php echo $utmctr; ?>" />
                    <input type="hidden" name="utm_content" value="<?php echo $utm_content; ?>" />
                    <!-- UTM Data -->
                </form>
            </div>
        </section>

        <section>
            <p class="copyright">
                COPYRIGHT <?php echo date('Y'); ?> . D2M. All rights reserved
            </p>
        </section>

    </div>


    <!-- Common Thing for Lp -->

    <div id="ohsnap"></div>


    <div class="stickyForm" id="stickyFormWrap" autocomplete="off">
        <button class="btn">Enquire Now</button>
        <form action="saveInfo.php" id="stickForm" method="post" autocomplete="off" class="form_sticky">
            <label for="name">
                <input type="text" name="name" id="name" placeholder="Name" required="" />
            </label>
            <label for="email">
                <input type="email" name="email" id="email" placeholder="Email Id" required="" />
            </label>
            <label for="phone">
                <input type="hidden" name="country_code" id="country_code_sticky_form" value="91" />
                <input type="text" name="phone" id="phone" placeholder="Phone Number" minlength="5" maxlength="15" required="" class="phone" />
            </label>
            </label>
            <label for="phone">
                <input type="text" name="message" placeholder="Message" required>
            </label>
            <div class="btn-group">
                <button type="submit" name="submit" id="button" class="_animate">Submit</button>
                <div class="loading" style="display: none;"> <img src="assets/images/loading.gif" alt="">
                </div>
            </div>

            <!-- UTM Data -->
            <input type="hidden" name="form_name" value="Sticky Form" />
            <input type="hidden" name="project" value="D2M Landing Page" />
            <input type="hidden" name="utm_source" value="<?php echo $utm_source; ?>" />
            <input type="hidden" name="utm_medium" value="<?php echo $utm_medium; ?>" />
            <input type="hidden" name="utm_campaign" value="<?php echo $utm_campaign; ?>" />
            <input type="hidden" name="matchtype" value="<?php echo $utm_matchtype; ?>" />
            <input type="hidden" name="keyword" value="<?php echo $utm_keyword; ?>" />
            <input type="hidden" name="device" value="<?php echo $utm_device; ?>" />
            <input type="hidden" name="placement" value="<?php echo $utm_placement; ?>" />
            <input type="hidden" name="pageUrl" value='<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>' />
            <input type="hidden" name="gclid" value="<?php echo $gclid; ?>" />
            <input type="hidden" name="original_referrer" value="<?php echo $original_referrer; ?>" />
            <input type="hidden" name="utmctr" value="<?php echo $utmctr; ?>" />
            <input type="hidden" name="utm_content" value="<?php echo $utm_content; ?>" />
            <!-- UTM Data -->
        </form>
    </div>

    <!-- Common Thing for Lp -->



    <!-- scripts works here -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>');
    </script>
    <script type="rocketlazyloadscript" src="assets/js/bootstrap.min.js"></script>
    <script type="rocketlazyloadscript" src="assets/js/22-wow.min.js"></script>
    <script type="rocketlazyloadscript" src="assets/js/aos.js"></script>
    <script type="rocketlazyloadscript" src="assets/js/magnific-popup.js"></script>
    <script type="rocketlazyloadscript" src="assets/js/ohsnap.min.js"></script>
    <script type="rocketlazyloadscript" src="assets/js/owl.carousel.min.js"></script>
    <script src="//cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script type="rocketlazyloadscript" src="assets/js/intlTelInput.min.js?<?php echo rand(100, 1000); ?>"></script>
    <script type="rocketlazyloadscript" src="assets/js/main.js?<?php echo rand(100, 1000); ?>"></script>
    <script>
        /* Form Validation */
        $('form#stickForm, form#footer_form').each(function() {
            console.log()
            var form = $(this);
            if (form.length) {
                form.validate({
                    rules: {
                        phone: {
                            required: true,
                            number: true,
                        },
                        email: {
                            required: true,
                            email: true
                        }
                    },
                    submitHandler: function(form) {
                        console.log(form.id);
                        var btn = $('#' + form.id + ' [type="submit"]'),
                            _form = $(form),
                            loading = _form.find('.loading');
                        loading.fadeIn(), btn.attr('disabled', ''), _form.addClass('disabled')
                        $.ajax({
                            url: form.action,
                            type: form.method,
                            data: $(form).serialize(),
                            success: function(data) {
                                loading.fadeOut(), btn.removeAttr('disabled'), _form.removeClass('disabled')
                                if (data) {
                                    try {
                                        data = jQuery.parseJSON(data);
                                    } catch (e) {
                                        console.log(e) // error in the above string (in this case, yes)!
                                    }
                                }
                                response = data.response;
                                console.log(data);
                                if (response.code == 0)
                                    ohSnap('Failed sending your informations, please try again!', {
                                        color: 'red'
                                    });
                                else if (response.code == 1) {

                                    // form.reset()

                                    ohSnap('Your information successfully reached us.', {
                                        color: 'green',
                                        'duration': '1000'
                                    }); // 

                                    let email = $(form).find("input[type='email']").val();

                                    setTimeout(function() {
                                        window.location = 'thankyou.php';
                                    }, 1000);

                                } else if (response.code == 2) {
                                    ohSnap('User already exists!', {
                                        color: 'green'
                                    });
                                } else
                                    ohSnap('Technical Error: Please contact administrator!', {
                                        color: 'red'
                                    });
                            }
                        });
                        return false;
                    }
                });
            }
        });
    </script>
</body>

</html>