<?php

include('../config.php');

if (isset($_GET['filter'])) {

    $filter = $_GET['filter'];

    try {

        $data = array();

        if ($_GET['filter'] == "admin") {

            $sql = "SELECT * FROM " . _TABLE_NAME_ . " order by last_updated_time desc;";

            // $result = mysqli_query($conn, $sql);
            $result = $conn->query($sql);
            while ($row = mysqli_fetch_assoc($result)) {
                $data[] = $row;
            }
        }

        echo json_encode(array('data' => $data));
        
    } catch (Exception $e) {
        http_response_code(400);
        $error_array['msg'] = $e->getMessage();
        echo json_encode(
            $error_array
        );
    }
} else {
    http_response_code(400);
    $error_array['msg'] = "No Access";
    echo json_encode(
        $error_array
    );
}
