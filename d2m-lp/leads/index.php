<?php
 // error_reporting(E_ERROR | E_PARSE);
include('../config.php');

if(!isset($_SESSION['project_name']) || empty($_SESSION['project_name'])){
    include 'login.php';
    exit;
}
else{
    $projectSrdVal = $_SESSION['project_name'];
}


?>

<!DOCTYPE html>

<html>
    <head>
        <title><?php echo LP_NAME ?> - <?php echo $_SESSION['project_name']; ?> Enquiry Details</title>

        <link rel="shortcut icon" href="images/fav-icon.png" type="image/x-icon" />

        <!-- CSS Start-->

        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all" />

        <link rel="stylesheet" href="css/jquery.dataTables.min.css" type="text/css" media="all" />

        <link rel="stylesheet" href="css/semantic.min.css" type="text/css" media="all" />

        <link rel="stylesheet" href="css/dataTables.semanticui.min.css" type="text/css" media="all" />

        <meta name="robots" content="nofollow ,noindex" />

        <!-- CSS End-->

        <!-- jQuery Start-->

        <script type="text/javascript" charset="utf8" src="js/jquery-1.12.4.js"></script>

        <script type="text/javascript" charset="utf8" src="js/bootstrap.min.js"></script>

        <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>

        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>

        <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>

        <script type="text/javascript" src="js/dataTables.semanticui.min.js"></script>

        <!-- jQuery End-->

        <style> @font-face {font-family: Icons; src: url("../font/icons.woff2"); } body {overflow: visible; } tfoot input {width: 100%; padding: 3px; box-sizing: border-box; } div.wrapper {padding: 20px; } .ui.pagination.menu .active.item, .ui.table thead th {background: #f5f5f5; color: black !important; } table.dataTable.no-footer {border-bottom-color: rgba(34, 36, 38, 0.1); } .ui.link.menu .item:hover, .ui.menu .dropdown.item:hover, .ui.menu .link.item:hover, .ui.menu a.item:hover {background: #9e9e9e; color: white; } select {color: black; font-weight: normal; } select option {color: black !important; } .exportExcel {padding: 5px; border: 1px solid grey; margin: 5px; cursor: pointer; } .dt-buttons a {background: #607d8b; padding: 8px 20px; color: white; width: 100px; margin-right: 10px; border-radius: 3px; outline: 0; } .dt-buttons {display: inline-block; margin: 20px auto; } .ui.pagination.menu {margin: 10px 0px; } </style>
        <style> @font-face {font-family: Icons; src: url("fonts/icons.woff2"); } body {overflow: visible; } tfoot input {width: 100%; padding: 3px; box-sizing: border-box; } div.wrapper {padding: 20px; } .ui.pagination.menu .active.item, .ui.table thead th {background: #f5f5f5; color: black !important; } table.dataTable.no-footer {border-bottom-color: rgba(34, 36, 38, 0.1); } .ui.link.menu .item:hover, .ui.menu .dropdown.item:hover, .ui.menu .link.item:hover, .ui.menu a.item:hover {background: #9e9e9e; color: white; } select {color: black; font-weight: normal; } select option {color: black !important; } .exportExcel {padding: 5px; border: 1px solid grey; margin: 5px; cursor: pointer; } .dt-buttons a {background: #607d8b; padding: 8px 20px; color: white; width: 100px; margin-right: 10px; border-radius: 3px; outline: 0; } .dt-buttons {display: inline-block; margin: 20px auto; } .ui.pagination.menu {margin: 10px 0px; } /*.leads_table_filter input[type=search]{box-shadow: none; border: 1px solid #9c9696; padding: 10px; border-radius: 4px; }*/ .view_btn {background-color: #2d596f; padding: 6px 15px; color: white; cursor: pointer; /* margin: 10px; */ border-radius: 5px; line-height: 49px; } .logoutbutton {background: #e04545; color: white; padding: 7px 20px; border-radius: 4px; text-decoration: none; text-transform: uppercase; position: fixed; right: 46px; top: 9px; } .lead-table {display: none; } </style> </head>

    <body>
        <div class="wrapper">
            <h3><?php echo LP_NAME ?> - Enquiry Details</h3>

            <div class="logout">
                <a href="logout.php" class="logoutbutton">Logout</a>
            </div>

            <table id="leads_table" class="ui celled table lead-table" cellspacing="0" width="100%">
                <thead>
                    <tr>  
                        <th>S.No</th> 
                        <th>Name</th> 
                        <th>Email</th> 
                        <th>Phone</th>  
                        <th>Message</th>  
                        <th>Form Name</th> 
                        <th>Date</th>
                        <th>Page URL</th>
                        <th>UTM Source</th>
                        <th>UTM Medium</th>
                        <th>UTM Term</th>
                        <th>UTM Content</th>
                        <th>UTM Campaign</th>
                        <th>IP Address</th>
                    </tr>
                </thead>
            </table>
        </div>

        <script>
            (function ($) {
                "use strict";

                function dataTable() {
                    var i = 1;

                    $("#leads_table").show();

                    $("#leads_table").DataTable({
                        ajax: {
                            url: "get_data.php",
                            type: "get",
                            data: {
                                filter: "<?php echo $projectSrdVal; ?>",
                            },
                        },
                        columns: [
                            {
                                render: function (data, type, full, meta) {
                                    return i++;
                                },
                            },
                            {
                                data: "name",
                                className: "col-name",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "email",
                                className: "col-app text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "phone",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "message",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "form_name",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "last_updated_time",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "page_url",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "utm_source",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "utm_medium",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "utm_term",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "utm_content",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "utm_campaign",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                            {
                                data: "ip_address",
                                className: "col-name text-left",
                                orderable: true,
                                searchable: true,
                            },
                        ],
                        dom: "Bfrtip",
                        buttons: [
                             { extend: 'copy', text: 'COPY TABLE' },
                              { extend: 'csv', text: 'EXPORT TABLE TO CSV' },
                               { extend: 'excel', text: 'EXPORT TABLE TO EXCEL' }
                        ],
                        destroy: true,
                        pageLength: 5,
                        pagingType: 'full_numbers',
                    });
                }

                dataTable();
            })(jQuery);
            
        </script>

        <?php mysqli_close($conn); ?>
    </body>
</html>
