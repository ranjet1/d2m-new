<?php 
error_reporting(E_ERROR | E_PARSE);

if(!isset($conn))
  require_once('config.php');

if(isset($_SESSION['user']) && !empty($_SESSION['user'])){ 

  header("location: index.php");
  exit;

}

if (isset($_POST['username']) && isset($_POST['password'])){

  $username = filter_var($_POST["username"], FILTER_SANITIZE_STRING);
  $password = $_POST["password"];

        $users = ['all_leads'];

        $leadsCredential = array(
          'admin' => "7SRfZbjxHQ3xH10G", 
        );

     if (isset($username) && isset($password)) {
      if ($leadsCredential[$username] == $password) {
        $_SESSION['project_name'] = $username;
        header('Location: index.php');
      }
      else{
        $error = 'Login incorrect!';
      }
     }
  else
    $error = 'Login incorrect!';
  }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Login Page | <?php echo LP_NAME ?></title>
  <link rel="stylesheet" href="css/login_style.css">
  <meta name="robots" content="nofollow ,noindex" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript">
     $("#login-button").click(function(event){
      event.preventDefault();
       
       $('form').fadeOut(500);
       $('.wrapper').addClass('form-success');
    });
  </script>
</head>
<body>
  <div class="wrapper">
    <div class="container">
      <h1><?php echo LP_NAME ?> <br> Login</h1>
      <?php if(isset($error) && $error!=''): ?>
      <div class="alert"><?php echo $error ?></div>
      <?php endif; ?>
      <form class="form" method="post" action="" autocomplete="off">
        <select name="username">
          <option value="admin"><?php echo LP_NAME ?> Leads</option>
        </select>
        <input type="password" placeholder="Password" name="password" />
        <button type="submit" id="login-button">Login</button>
      </form>
    </div>
    
    <ul class="bg-bubbles">
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
    </ul>
  </div>
  
</body>
</html>