Installation Instructions
-------------------------

Step 1: Transfer all the files to the server
Step 2: import the "db_table_sql_file.sql" file
Step 3: Open "Config.php" file and replace 
    - "DB_NAME" with your server database name
    - "DB_USER" with your database user
    - "DB_PASS" with you database user's password
    - "_TABLE_NAME_" with your table created on the database while importing the sql file.


-----------------------------------------------------------------------------------------------


To access the Leads Dashboard, use url of the landing page and add "/leads" at the end of the url

Example : www.example.com/leads


Use the below password to login in to the Leads Dashboard.

Password : 7SRfZbjxHQ3xH10G