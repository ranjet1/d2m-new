<?php
include('config.php');

function saveReference($data, $file)
{
	global $db, $conn;
	$name = (isset($data['name']) ? $data['name'] : false);
	$phone = (isset($data['phone']) ? $data['phone'] : false);
	$email = (isset($data['email']) ? $data['email'] : false);
	$message = (isset($data['message']) ? $data['message'] : '');
	$form_name = (isset($data['form_name']) ? $data['form_name'] : '');
	$utm_source = (isset($data['utm_source']) ? $data['utm_source'] : '');
	$utm_medium = (isset($data['utm_medium']) ? $data['utm_medium'] : '');
	$utm_term = (isset($data['utm_term']) ? $data['utm_term'] : '');
	$utm_content = (isset($data['utm_content']) ? $data['utm_content'] : '');
	$utm_campaign = (isset($data['utm_campaign']) ? $data['utm_campaign'] : '');
	$pageUrl = (isset($data['pageUrl']) ? $data['pageUrl'] : '');

	$ip_address = $data['ip_address'] = get_client_ip();

	// $isUserExistsQry = mysqli_query($conn, "SELECT email FROM "._TABLE_NAME_." WHERE `email` = '$email'");
	// $isUserExists = mysqli_fetch_array($isUserExistsQry);
	// $userExist = $isUserExists['email'];

	if (false)
		$response = showResponse(2, 'user already exists');
	else {
		if ($name && $phone) {

			$dateTime = date('Y-m-d H:i:s');

			$result = mysqli_query($conn, "INSERT INTO " . _TABLE_NAME_ . "(`name`, `email`, `phone`, `message`, `form_name`, `utm_source`, `utm_medium`, `utm_term`, `utm_content`, `utm_campaign`, `page_url`,  `ip_address`, `last_updated_time` ) VALUES ('$name','$email','$phone', '$message', '$form_name', '$utm_source', '$utm_medium', '$utm_term', '$utm_content', '$utm_campaign', '$pageUrl', '$ip_address', '$dateTime' )");

			if ($result) {
				$response = showResponse(1, 'informations successfully saved');
			} else
				$response = showResponse(0, 'failed to saved info' . mysqli_error($conn));
		} else
			$response = showResponse(-1, 'data insuffient');
	}
	return $response;
}

// Function to get the client IP address
function get_client_ip()
{
	$ipaddress = '';
	if (getenv('HTTP_CLIENT_IP'))
		$ipaddress = getenv('HTTP_CLIENT_IP');
	else if (getenv('HTTP_X_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	else if (getenv('HTTP_X_FORWARDED'))
		$ipaddress = getenv('HTTP_X_FORWARDED');
	else if (getenv('HTTP_FORWARDED_FOR'))
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
	else if (getenv('HTTP_FORWARDED'))
		$ipaddress = getenv('HTTP_FORWARDED');
	else if (getenv('REMOTE_ADDR'))
		$ipaddress = getenv('REMOTE_ADDR');
	else
		$ipaddress = 'UNKNOWN';
	return $ipaddress;
}


function showResponse($response_code, $response_message)
{
	header('Content-Type: application/json');
	$responseArray = array(
		"code" => $response_code,
		"message" => $response_message
	);
	$responseArray = array("response" => $responseArray);
	return stripslashes(json_encode($responseArray, 128));
}

echo saveReference($_POST, $_FILES);

?>
 <?php mysqli_close($conn); ?>