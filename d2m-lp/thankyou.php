<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thank You Page</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="images/fav-icon.png">

    <!-- Stylesheets
    ================================================== -->
    <!-- <link rel="stylesheet" href="css/bootstrap_grid.css"> -->
    <link rel="stylesheet" href="css/style.css">
</head>

<style type="text/css">
    body * {
        font-family: "Poppins", sans-serif;
    }

    .thankyou-page ._header {
        background: #e4f3e5;
        padding: 100px 30px;
        text-align: center;
        background: #e4f3e5 url(https://codexcourier.com/images/main_page.jpg) center/cover no-repeat;
    }

    .thankyou-page ._header .logo {
        max-width: 200px;
        margin: 0 auto 50px;
    }

    .thankyou-page ._header .logo img {
        width: 100%;
    }

    .thankyou-page ._header h1 {
        font-size: 65px;
        font-weight: 800;
        color: #42ad4f;
        margin: 0;
    }

    .thankyou-page ._body {
        margin: -70px 0 30px;
    }

    .thankyou-page ._body ._box {
        margin: auto;
        max-width: 80%;
        padding: 50px;
        background: white;
        border-radius: 3px;
        box-shadow: 0 0 35px rgba(10, 10, 10, 0.12);
        -moz-box-shadow: 0 0 35px rgba(10, 10, 10, 0.12);
        -webkit-box-shadow: 0 0 35px rgba(10, 10, 10, 0.12);
    }

    .thankyou-page ._body ._box h2 {
        font-size: 40px;
        font-weight: 600;
        color: #4ab74a;
        text-align: center;
    }

    .thankyou-page ._body ._box p {
        text-align: center;
    }

    .thankyou-page ._footer {
        text-align: center;
        padding: 50px 30px;
    }

    .thankyou-page ._footer .btn {
        background: #4ab74a;
        color: white;
        border: 0;
        font-size: 14px;
        font-weight: 600;
        border-radius: 0;
        letter-spacing: 0.8px;
        padding: 20px 33px;
        text-transform: uppercase;
    }
</style>

<body>

    <div class="thankyou-page">
        <div class="_header">
            <div class="logo">
                <img src="assets/images/logo.png" alt="">
            </div>

        </div>
        <div class="_body">
            <div class="_box">
                <h2>Thank you for your interest!</h2>
                <p>We will get back to you soon.</p>
            </div>
        </div>
        <div class="_footer">
            <!-- <p>Having trouble? <a href="">Contact us</a> </p> -->
            <a class="btn" href="index.php">Back to homepage</a>
        </div>
    </div>

    <!-- scripts works here -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/jquery-1.11.1.min.js"><\/script>')
    </script>
    <script src="js/jquery.jgrowl.min.js"></script>

</body>

</html>