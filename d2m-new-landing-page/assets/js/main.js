$(document).ready(function () {
  $(".phone").intlTelInput({
    defaultCountry: "auto",
    initialCountry: "auto",
    geoIpLookup: function (callback) {
      callback("IN");
    },
    utilsScript: "assets/js/utils.js",
  }),
    $(".phone").on("countrychange", function (e, countryData) {
      var dialCode = countryData.dialCode,
        countryCodeElem = $(
          "#country_code_sticky_form, #country_code_banner_form, #country_code_banner_bottom, #country_code_brochure_form"
        );
      countryCodeElem.val(dialCode);
    });

  // video popup
  $(".video-link").magnificPopup({
    type: "iframe",
    // other options
  });
  $("#brochure-download-form").magnificPopup({
    type: "iframe",
    // other options
  });

  // content popup
  $(".popup-link").magnificPopup({
    type: "inline",
    fixedContentPos: true,
    // other options
  });

  $(".image-popup-no-margins").magnificPopup({
    type: "image",
    closeOnContentClick: true,
    closeBtnInside: false,
    fixedContentPos: true,
    mainClass: "mfp-no-margins mfp-with-zoom", // class to remove default margin from left and right side
    image: {
      verticalFit: true,
    },
    zoom: {
      enabled: true,
      duration: 300, // don't foget to change the duration also in CSS
    },
  });

  /* Form Validation */
  $("form").each(function () {
    console.log();
    var form = $(this);
    if (form.length) {
      form.validate({
        rules: {
          phone: {
            required: true,
            number: true,
          },
          email: {
            required: true,
            email: true,
          },
        },
        submitHandler: function (form) {
          console.log(form.id);
          var btn = $("#" + form.id + ' [type="submit"]'),
            _form = $(form),
            loading = _form.find(".loading");
          loading.fadeIn(),
            btn.attr("disabled", ""),
            _form.addClass("disabled");
          $.ajax({
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function (data) {
              loading.fadeOut(),
                btn.removeAttr("disabled"),
                _form.removeClass("disabled");
              if (data) {
                try {
                  data = jQuery.parseJSON(data);
                } catch (e) {
                  console.log(e); // error in the above string (in this case, yes)!
                }
              }
              response = data.response;
              console.log(data);
              if (response.code == 0)
                ohSnap("Failed sending your informations, please try again!", {
                  color: "red",
                });
              else if (response.code == 1) {
                form.reset();
                ohSnap("Your information successfully reached us.", {
                  color: "green",
                  duration: "1000",
                });
                setTimeout(function () {
                  window.location = "thankyou.php";
                }, 1000);
              } else if (response.code == 2) {
                ohSnap("User already exists!", { color: "green" });
              } else
                ohSnap("Technical Error: Please contact administrator!", {
                  color: "green",
                });
            },
          });
          return false;
        },
      });
    }
  });

  // owl carousel
  $(".owl-home-slider").owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    lazyLoad: true,
    dots: false,
    autoplay: false,
    smartSpeed: 500,
    navText: [
      '<i class="fa fa-angle-left" aria-hidden="true"></i>',
      '<i class="fa fa-angle-right" aria-hidden="true"></i>',
    ],
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 1,
      },
    },
  });

  // owl carousel

  // owl carousel
  $(".owl-gallery-slider").owlCarousel({
    loop: true,
    nav: true,
    margin: 10,
    dots: true,
    autoplay: false,
    smartSpeed: 1500,
    navText: ["PREV", "NEXT"],
    center: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      768: {
        items: 3,
      },
      1000: {
        items: 5,
      },
      1200: {
        items: 5,
      },
    },
  });
  $(".owl-video-slider").owlCarousel({
    loop: true,
    nav: true,
    margin: 10,
    dots: true,
    autoplay: true,
    smartSpeed: 1500,
    navText: [
      '<i class="fa fa-angle-left" aria-hidden="true"></i>',
      '<i class="fa fa-angle-right" aria-hidden="true"></i>',
    ],
    center: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      768: {
        items: 3,
      },
      1000: {
        items: 5,
      },
      1200: {
        items: 5,
      },
    },
  });
  // owl carousel
  $(".owl_video_slider").owlCarousel({
    loop: true,
    margin: 20,
    nav: false,
    dots: true,
    smartSpeed: 2500,
    navText: [
      '<i class="fa fa-angle-left" aria-hidden="true"></i>',
      '<i class="fa fa-angle-right" aria-hidden="true"></i>',
    ],
    center: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 1,
      },
    },
  });

  // owl carousel
  $(".owl-drone-slider").owlCarousel({
    loop: true,
    margin: 20,
    dots: false,
    smartSpeed: 2500,
    nav: true,
    navText: [
      '<i class="fa fa-angle-left" aria-hidden="true"></i>',
      '<i class="fa fa-angle-right" aria-hidden="true"></i>',
    ],
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 1,
      },
    },
  });

  // owl carousel
  // $(".owl_video_slider_thumb").owlCarousel({
  //   loop: true,
  //   margin: 40,
  //   nav: false,
  //   dots: true,
  //   animateOut: "slideOutUp",
  //   animateIn: "slideInUp",
  //   mouseDrag: false,
  //   touchDrag: false,
  //   smartSpeed: 2500,
  //   autoplay: true,
  //   center: true,
  //   navText: [
  //     '<i class="fa fa-angle-left" aria-hidden="true"></i>',
  //     '<i class="fa fa-angle-right" aria-hidden="true"></i>',
  //   ],
  //   responsive: {
  //     0: {
  //       items: 1,
  //     },
  //     600: {
  //       items: 1,
  //     },
  //     1000: {
  //       items: 1,
  //     },
  //   },
  // });

  $(".menu li a").on("click", function () {
    $(".menu li a").removeClass("active");
    $(this).addClass("active");
  });
  $(".stickyForm .btn, .i-am, span.close_btn").on("click", function () {
    $(".stickyForm").toggleClass("active");
  });

  $(window).scroll(function () {
    if ($(window).scrollTop() >= 100) {
      $("header").addClass("fixed-header wow animated slideInDown");
    } else {
      $("header").removeClass("fixed-header wow animated slideInDown");
    }
  });

  //anchor scroll smooth
  $('a[href*="#"]:not([href="#"])')
    .not(".tabs a, ._tabs a, .panel-title a")
    .click(function () {
      if (
        location.pathname.replace(/^\//, "") ==
          this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        var target = $(this.hash);
        target = target.length
          ? target
          : $("[name=" + this.hash.slice(1) + "]");
        if (target.length) {
          $("html, body").animate(
            {
              scrollTop: target.offset().top - 70,
            },
            1000
          );
          return false;
        }
      }
    });

  $(".read_more_btn").on("click", function () {
    $(this).hide();
    $(".read_more_content").slideDown();
  });
  $(".read_less_btn").on("click", function () {
    $(".read_more_content").slideUp();

    setTimeout(function () {
      $(".read_more_btn").show();
    }, 300);
  });

  $(".tab_buttons li").on("click", function () {
    $(".tab_content").removeClass("active animated wow fadeInUp");
    $(".tab_buttons li").removeClass("active");

    $(this).addClass("active");

    let target = $(this).data("target");

    $(target).addClass("active animated wow fadeInUp");
  });
  // new
  $(".master_plan-section .nav-link.active").on("click", function () {
    $("#nav-tab").addClass("active");
    $(".tab_buttons li").removeClass("active");

    $(this).addClass("active");

    let target = $(this).data("target");

    $(target).addClass("active");
  });
  $(".table_enquire_form_btn").magnificPopup({
    items: {
      src: "#table-enquire-form",
      type: "inline",
    },
  });

  $(".table_enquire_form_btn").on("click", function () {
    $("#table-enquire-popup-form .capture_id_btn").attr("id", "");
    form_key_value = $(this).attr("data-key");
    var clickvalue = $("#table-enquire-popup-form .capture_id_btn").attr(
      "id",
      form_key_value
    );
    console.log(clickvalue);
  });
});

$(".owl-unit-plan-slider").owlCarousel({
  loop: true,
  margin: 10,
  dots: false,
  smartSpeed: 2500,
  nav: true,
  navText: [
    '<i class="fa fa-angle-left" aria-hidden="true"></i>',
    '<i class="fa fa-angle-right" aria-hidden="true"></i>',
  ],
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1000: {
      items: 1,
    },
  },
});
$("#mobile-highlights-slider").owlCarousel({
  loop: true,
  margin: 0,
  dots: true,
  smartSpeed: 2500,
  nav: true,
  navText: [
    '<i class="fa fa-angle-left" aria-hidden="true"></i>',
    '<i class="fa fa-angle-right" aria-hidden="true"></i>',
  ],
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1000: {
      items: 1,
    },
  },
});
$("#mobile-aminities-slider").owlCarousel({
  loop: true,
  margin: 0,
  dots: false,
  smartSpeed: 2500,
  nav: true,
  navText: [
    '<i class="fa fa-angle-left" aria-hidden="true"></i>',
    '<i class="fa fa-angle-right" aria-hidden="true"></i>',
  ],
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1000: {
      items: 1,
    },
  },
});
$(".zoom-image-popup").magnificPopup({
  type: "image",
  fixedContentPos: true,
  callbacks: {
    open: function () {
      var $image = $(".mfp-img");
      $image.CloudZoom({
        zoomPosition: "inside",
        zoomOffsetX: 0,
      });
    },
    close: function () {
      var $image = $(".mfp-img");
      if ($image.data("CloudZoom")) $image.data("CloudZoom").destroy();
    },
  },
});
$(".location_highlights_tabs button").on("click", function (e) {
  e.preventDefault();
  $(".location_highlights_tabs button").removeClass("active");
  $(".tab_location_highlights_content").removeClass(
    "active animated wow fadeInUp"
  );
  var target = $(this).attr("data-target");
  $(this).addClass("active");
  $(target).addClass("active animated wow fadeInUp");
});
// new

$(window).scroll(function () {
  var position = window.pageYOffset;
  $(".section").each(function () {
    var target = $(this).offset().top - 450;

    var id = $(this).attr("id");
    if (position >= target) {
      if (
        id == "hero_banner_section" ||
        id == "banner-form" ||
        id == "about" ||
        id == "footer-form"
      ) {
        $("#sticky_form_btn").hide();
      } else {
        $("#sticky_form_btn").show();
      }
    }
  });
});

$(".phone").intlTelInput({
  defaultCountry: "auto",
  initialCountry: "auto",
  geoIpLookup: function (callback) {
    callback("IN");
  },
  utilsScript: "./assets/js/utils.js",
}),
  $(".phone").on("countrychange", function (e, countryData) {
    var dialCode = countryData.dialCode,
      countryCodeElem = $(
        "#country_code"
      );
    countryCodeElem.val(dialCode);
  });
