<?php
$utm_source = (isset($_GET['utm_source']) ? $_GET['utm_source'] : '');
$utm_medium = (isset($_GET['utm_medium']) ? $_GET['utm_medium'] : '');
$utm_campaign = (isset($_GET['utm_campaign']) ? $_GET['utm_campaign'] : '');
$utm_content = (isset($_GET['utm_content']) ? $_GET['utm_content'] : '');

// $original_referrer = $_SERVER['HTTP_REFERER'];
$original_referrer = '';
if (!$original_referrer) {
    $original_referrer = "Direct";
} else {
    $parse = parse_url($original_referrer);
    $original_referrer = $parse['host'];
}
$utmctr = (isset($_GET['utmctr']) ? $_GET['utmctr'] : '');
// New Parameters
$pageUrl = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$user_agent = $_SERVER['HTTP_USER_AGENT'];
$devices = ["lighthouse", "light house", "google-amp", "google amp", "page-speed", "page speed"];
$crawlagent = false;
if ($user_agent)
    foreach ($devices as $device) {
        $isMatch = (bool) preg_match("/" . $device . "/i", $user_agent);
        if ($isMatch) {
            $crawlagent =  $device;
            break;
        }
    }
if (isset($_GET['debug']))
    $crawlagent = 'agent';
?>

<!DOCTYPE html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>d2m</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <link rel="canonical" href="" />
    <meta name="description" content="" />
    <!-- <meta name="robots" content="noindex"> -->
    <link rel="shortcut icon" href="./assets/images/D2m-logo-fevicon.ico" />
    <link rel="apple-touch-icon" sizes="32x32" href="./assets/images/D2m-logo-fevicon.ico" />
    <link rel="apple-touch-icon" sizes="76x76" href="./assets/images/D2m-logo-fevicon.ico" />
    <link rel="apple-touch-icon" sizes="120x120" href="./assets/images/D2m-logo-fevicon.ico" />
    <link rel="apple-touch-icon" sizes="152x152" href="./assets/images/D2m-logo-fevicon.ico" />

    <?php if (!$crawlagent) : ?>
        <script type="text/javascript">
            let agg = navigator.userAgent;
            var __ac_ = agg.includes("Lighthouse");
        </script>
        <script type="text/javascript">
            scripts_loaded = false;
            class RocketLazyLoadScripts {
                constructor(e) {
                    (this.triggerEvents = e),
                    (this.eventOptions = {
                        passive: !0,
                    }),
                    (this.userEventListener = this.triggerListener.bind(this)),
                    (this.delayedScripts = {
                        normal: [],
                        async: [],
                        defer: [],
                    }),
                    (this.allJQueries = []);
                }
                _addUserInteractionListener(e) {
                    this.triggerEvents.forEach((t) => window.addEventListener(t, e.userEventListener, e.eventOptions));
                }
                _removeUserInteractionListener(e) {
                    this.triggerEvents.forEach((t) => window.removeEventListener(t, e.userEventListener, e.eventOptions));
                }
                triggerListener() {
                    this._removeUserInteractionListener(this), "loading" === document.readyState ? document.addEventListener("DOMContentLoaded", this._loadEverythingNow.bind(this)) : this._loadEverythingNow();
                }
                async _loadEverythingNow() {
                    if (!scripts_loaded) {
                        scripts_loaded = true;
                        console.log("loading now");
                        this._delayEventListeners(),
                            this._delayJQueryReady(this),
                            this._handleDocumentWrite(),
                            this._registerAllDelayedScripts(),
                            this._preloadAllScripts(),
                            await this._loadScriptsFromList(this.delayedScripts.normal),
                            await this._loadScriptsFromList(this.delayedScripts.defer),
                            await this._loadScriptsFromList(this.delayedScripts.async),
                            await this._triggerDOMContentLoaded(),
                            await this._triggerWindowLoad(),
                            window.dispatchEvent(new Event("rocket-allScriptsLoaded"));
                    }
                }
                _registerAllDelayedScripts() {
                    document.querySelectorAll("script[type=rocketlazyloadscript]").forEach((e) => {
                        e.hasAttribute("src") ?
                            e.hasAttribute("async") && !1 !== e.async ?
                            this.delayedScripts.async.push(e) :
                            (e.hasAttribute("defer") && !1 !== e.defer) || "module" === e.getAttribute("data-rocket-type") ?
                            this.delayedScripts.defer.push(e) :
                            this.delayedScripts.normal.push(e) :
                            this.delayedScripts.normal.push(e);
                    });
                }
                async _transformScript(e) {
                    return (
                        await this._requestAnimFrame(),
                        new Promise((t) => {
                            const n = document.createElement("script");
                            let i;
                            [...e.attributes].forEach((e) => {
                                    let t = e.nodeName;
                                    "type" !== t && ("data-rocket-type" === t && ((t = "type"), (i = e.nodeValue)), n.setAttribute(t, e.nodeValue));
                                }),
                                e.hasAttribute("src") ? (n.addEventListener("load", t), n.addEventListener("error", t)) : ((n.text = e.text), t()),
                                e.parentNode.replaceChild(n, e);
                        })
                    );
                }
                async _loadScriptsFromList(e) {
                    const t = e.shift();
                    return t ? (await this._transformScript(t), this._loadScriptsFromList(e)) : Promise.resolve();
                }
                _preloadAllScripts() {
                    var e = document.createDocumentFragment();
                    [...this.delayedScripts.normal, ...this.delayedScripts.defer, ...this.delayedScripts.async].forEach((t) => {
                            const n = t.getAttribute("src");
                            if (n) {
                                const t = document.createElement("link");
                                (t.href = n), (t.rel = "preload"), (t.as = "script"), e.appendChild(t);
                            }
                        }),
                        document.head.appendChild(e);
                }
                _delayEventListeners() {
                    let e = {};

                    function t(t, n) {
                        !(function(t) {
                            function n(n) {
                                return e[t].eventsToRewrite.indexOf(n) >= 0 ? "rocket-" + n : n;
                            }
                            e[t] ||
                                ((e[t] = {
                                        originalFunctions: {
                                            add: t.addEventListener,
                                            remove: t.removeEventListener,
                                        },
                                        eventsToRewrite: [],
                                    }),
                                    (t.addEventListener = function() {
                                        (arguments[0] = n(arguments[0])), e[t].originalFunctions.add.apply(t, arguments);
                                    }),
                                    (t.removeEventListener = function() {
                                        (arguments[0] = n(arguments[0])), e[t].originalFunctions.remove.apply(t, arguments);
                                    }));
                        })(t),
                        e[t].eventsToRewrite.push(n);
                    }

                    function n(e, t) {
                        let n = e[t];
                        Object.defineProperty(e, t, {
                            get: () => n || function() {},
                            set(i) {
                                e["rocket" + t] = n = i;
                            },
                        });
                    }
                    t(document, "DOMContentLoaded"), t(window, "DOMContentLoaded"), t(window, "load"), t(window, "pageshow"), t(document, "readystatechange"), n(document, "onreadystatechange"), n(window, "onload"), n(window, "onpageshow");
                }
                _delayJQueryReady(e) {
                    let t = window.jQuery;
                    Object.defineProperty(window, "jQuery", {
                        get: () => t,
                        set(n) {
                            if (n && n.fn && !e.allJQueries.includes(n)) {
                                n.fn.ready = n.fn.init.prototype.ready = function(t) {
                                    e.domReadyFired ? t.bind(document)(n) : document.addEventListener("rocket-DOMContentLoaded", () => t.bind(document)(n));
                                };
                                const t = n.fn.on;
                                (n.fn.on = n.fn.init.prototype.on = function() {
                                    if (this[0] === window) {
                                        function e(e) {
                                            return e
                                                .split(" ")
                                                .map((e) => ("load" === e || 0 === e.indexOf("load.") ? "rocket-jquery-load" : e))
                                                .join(" ");
                                        }
                                        "string" == typeof arguments[0] || arguments[0] instanceof String ?
                                            (arguments[0] = e(arguments[0])) :
                                            "object" == typeof arguments[0] &&
                                            Object.keys(arguments[0]).forEach((t) => {
                                                delete Object.assign(arguments[0], {
                                                    [e(t)]: arguments[0][t],
                                                })[t];
                                            });
                                    }
                                    return t.apply(this, arguments), this;
                                }),
                                e.allJQueries.push(n);
                            }
                            t = n;
                        },
                    });
                }
                async _triggerDOMContentLoaded() {
                    (this.domReadyFired = !0),
                    await this._requestAnimFrame(),
                        document.dispatchEvent(new Event("rocket-DOMContentLoaded")),
                        await this._requestAnimFrame(),
                        window.dispatchEvent(new Event("rocket-DOMContentLoaded")),
                        await this._requestAnimFrame(),
                        document.dispatchEvent(new Event("rocket-readystatechange")),
                        await this._requestAnimFrame(),
                        document.rocketonreadystatechange && document.rocketonreadystatechange();
                }
                async _triggerWindowLoad() {
                    await this._requestAnimFrame(),
                        window.dispatchEvent(new Event("rocket-load")),
                        await this._requestAnimFrame(),
                        window.rocketonload && window.rocketonload(),
                        await this._requestAnimFrame(),
                        this.allJQueries.forEach((e) => e(window).trigger("rocket-jquery-load")),
                        window.dispatchEvent(new Event("rocket-pageshow")),
                        await this._requestAnimFrame(),
                        window.rocketonpageshow && window.rocketonpageshow();
                }
                _handleDocumentWrite() {
                    const e = new Map();
                    document.write = document.writeln = function(t) {
                        const n = document.currentScript,
                            i = document.createRange(),
                            r = n.parentElement;
                        let o = e.get(n);
                        void 0 === o && ((o = n.nextSibling), e.set(n, o));
                        const a = document.createDocumentFragment();
                        i.setStart(a, 0), a.appendChild(i.createContextualFragment(t)), r.insertBefore(a, o);
                    };
                }
                async _requestAnimFrame() {
                    return new Promise((e) => requestAnimationFrame(e));
                }
                static run(forceLoad) {
                    const e = new RocketLazyLoadScripts(["keydown", "mousemove", "touchmove", "touchstart", "touchend", "wheel"]);
                    if (forceLoad == "force") {
                        e._loadEverythingNow();
                        return;
                    }
                    e._addUserInteractionListener(e);
                }
            }
        </script>
        <script type="text/javascript">
            window.onload = function() {
                if (!__ac_) {
                    RocketLazyLoadScripts.run();
                }
                if (!__ac_) {
                    /* optimisation code 1*/

                    // lzyCSS("assets/css/animate.css");
                    lzyCSS("assets/css/owl.carousel.min.css");
                    lzyCSS("assets/css/magnific-popup.css");
                    lzyCSS("assets/css/ohsnap.css");
                    lzyCSS("assets/css/intlTelInput.css");
                    lzyCSS("assets/css/cloudzoom.css");

                    setTimeout(function() {
                        if (!scripts_loaded) {
                            RocketLazyLoadScripts.run("force");
                        }
                    }, 500);
                }
            };
        </script>
    <?php endif ?>
    <!-- optimisation code 3 -->
    <script type="text/javascript">
        var lzyCSS = function(href) {
            var l = document.createElement("link");
            l.rel = "stylesheet";
            l.href = href;
            var h = document.getElementsByTagName("head")[0];
            h.appendChild(l);
        };
    </script>

    <!-- Stylesheets
    ================================================== -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
    <link rel="stylesheet" href="./assets/fonts/font.css" />
    <!-- <link rel="stylesheet" href="./assets/css/aos.css" /> -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="./assets/css/style.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link rel="preconnect" href="//fonts.googleapis.com" />
    <link rel="preconnect" href="//fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="//fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet" />
    <!-- <link href="https://fonts.googleapis.com/css?family=Edensor" rel="stylesheet" /> -->


</head>

<body class="page">




    <main>
        <header>
            <div class="container">
                <div class="row">
                    <div class="d-flex justify-content-between align-items-center header-wrapper">
                        <div style="width: 100px;">
                            <div class="mobile_logo">
                                <a href="#">
                                    <img src="./assets/images/D2m-logo.png" alt="" />
                                </a>
                            </div>
                        </div>
                        <div class="d-flex align-items-center justify-content-end d-lg-none position-relative w-100">
                            <div class="top_menu_wrapper">
                                <a href="tel:01234567890" id="" class="call_us_btn">
                                    <span class=""><i class="fa-solid fa-phone"></i></span>
                                </a>
                                <div class="menu btn11" data-menu="11">
                                    <div class="icon-left"></div>
                                    <div class="icon-right"></div>
                                </div>
                            </div>

                        </div>

                        <div class="right_side_menu">
                            <ul class="menu">
                                <li>
                                    <a href="#overview">Overview</a>
                                </li>
                                <li>
                                    <a href="#choose">Why Choose?</a>
                                </li>
                                <li>
                                    <a href="#our-services">Our Services</a>
                                </li>
                                <li>
                                    <a href="#testimonials">Testimonials</a>
                                </li>
                                <li>
                                    <a href="#faq">FAQ's</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <article>
            <section class="banner-section">
                <div class="">
                    <div class="row direct">
                        <div class="containerBorder">
                            <div class="d2m-bannerImg">
                                <img src="assets/images/car2.webp" alt="Car Image">
                            </div>
                        </div>
                        <div class="d2m-bannerContent">
                            <div class="d2mContent">
                                <div class="contentHeading">
                                    <h2>Your All-In-One <br> Trusted Partner<span style="color: #42AD4F;"> For Cars!</span> </h2>
                                </div>
                                <div class="contentInfo">
                                    <p>Convenience and ultimate comfort, just a click away! Enjoy your long drives stress-free with D2MECH</p>
                                </div>
                                <div class="d2m-appLink">
                                    <div class="d2m-app">
                                        <a href="https://play.google.com/store/apps/details?id=com.app.d2m.user" target="_blank"><img src="assets/images/android-cta.png" alt="Google Play Store"></a>
                                    </div>
                                    <div class="d2m-app">
                                        <a href="https://apps.apple.com/in/app/d2mech/id1612037844" target="_blank">
                                            <img src="assets/images/iphone-cta.png" alt="Apple Store">
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>



                </div>
            </section>
            <section class="space-pd" id="overview">
                <div class="container">
                    <div class="row">
                        <div class="d2m-bannerBottom">
                            <div class="bannerBottom">
                                <div class="overviewContainer">
                                    <div class="overviewContent">
                                        <h2 class="overviewHeading">Overview</h2>
                                        <p>D2Mech is your one-stop destination in Ahmedabad for all things related to cars. From car rental and lease services to top-notch car repair and maintenance, we've got you covered!</p>
                                    </div>
                                    <div class="d2m-solution d-flex justify-content-around align-items-center">
                                        <div class="rental">
                                            <img src="assets/images/icon-1.png" alt="">
                                            <p>Reliable Long-Term Car Rental Solutions</p>
                                        </div>
                                        <div class="repair">
                                            <img src="assets/images/icon-2.png" alt="">
                                            <p>Quick & Efficient Car Repair - Maintenance</p>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="container" id="choose">
                    <div class="row">
                        <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-7 col-sm-12 col-xs-12 col-12 choose-section">
                            <h2>Why Choose D2MECH For Long-Term Car Rentals?</h2>
                            <div class="cards d-flex justify-content-sm-start justify-content-between align-items-start flex-wrap">
                                <div class="rental">
                                    <img src="assets/images/budget.png" alt="">
                                    <p>Budget Friendly</p>
                                </div>
                                <div class="rental">
                                    <img src="assets/images/tax.png" alt="">
                                    <p>Tax Savings</p>
                                </div>
                                <div class="rental">
                                    <img src="assets/images/diverse.png" alt="">
                                    <p>Diverse Car Models</p>
                                </div>
                                <div class="rental">
                                    <img src="assets/images/inclusive.png" alt="">
                                    <p>All-inclusive Driving</p>
                                </div>
                                <div class="rental">
                                    <img src="assets/images/hassle.png" alt="">
                                    <p>Hassle-Free Experience</p>
                                </div>



                            </div>
                        </div>
                        <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-5 col-sm-12 col-xs-12 col-12">
                            <img src="assets/images/phone-with-hand1.webp" alt="" class="hand-img">
                        </div>
                    </div>


                </div>

            </section>
            <section class="space-pd" id="our-services">
                <div class="container ">
                    <div class="row">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12">
                            <p class="title">Driving Care To Your Doorstep in Ahmedabad</p>
                            <h2 class="sub-title">Explore Our Services</h2>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 col-12 card-flex">
                            <div class="card">
                                <div class="content">
                                    <div class="front">
                                        <img src="assets/images/car-rental.png" alt="">
                                        <p>Long-Term Car Rentals</p>
                                    </div>
                                    <div class="back">
                                        <p>Drive your dream car without any commitment. D2MECH offers long-term car rental solutions at excellent and customisable packages.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="content">
                                    <div class="front">
                                        <img src="assets/images/car-maintenance.png" alt="">
                                        <p>Car Repair & Maintenance</p>
                                    </div>
                                    <div class="back">
                                        <p>Your one-stop shop for all your car care needs, be it painting service or replacing the oxygen sensor.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="row bg-color" id="statistics">
                        <div class="container">
                            <div class="row">
                                <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                                    <div class="statistics">
                                        <h3 class="butterfly_animation_title"><span id="counter_1">1000</span> <span>+</span></h3>
                                        <p>Number of cars serviced</p>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                                    <div class="statistics">
                                        <h3 class="butterfly_animation_title"><span id="counter_2">200</span> <span>+</span></h3>
                                        <p>Number of happy customers</p>
                                    </div>
                                </div>
                                <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                                    <div class="statistics">
                                        <h3 class="butterfly_animation_title"><span id="counter_3">4.2</span> </h3>
                                        <p>Average rating <br> We are trustworthy</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>



            </section>
            <section class="customers" id="testimonials">
                <div class="container">
                    <h2>What our customers <br> have to say?</h2>

                </div>
                <div class="container d-flex justify-content-around">
                    <div class="row">
                        <div class="card-item">
                            <p>I was always fascinated by the car renting model of the western countries. Finally, my friend, Apoorva Shah started D2Mech. Renting/Subscription model is hassel free.</p>
                            <span>Bhargav Trivedi - Gravity Marketing:</span>
                        </div>
                        <div class="card-item">
                            <p> I have had an amazing experience renting from D2Mech, as it is very convenient and they have a lot of good deals and great customer support. They will take care of everything.</p>
                            <span>Unmil Shah - VPL Diagnostic:</span>
                        </div>
                        <div class="card-item">
                            <p>I've had a fantastic experience renting a car through D2MECH Solutions in Ahmedabad. They took the hassle out of the entire process, from helping me find the perfect car for my needs to finalising the rental agreement.</p>
                            <span>Chirag Sheth - MEASHVI Technologies</span>
                        </div>
                    </div>

                </div>
            </section>
            <section id="faq" class="faq space-pd">
                <div class="container">
                    <h2>Frequently asked questions <br> about car services</h2>


                </div>
                <div class="accordion-wrapper" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                1. Does D2Mech provide long-term car rental services?
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Yes, D2Mech offers long-term car rental services, providing a hassle-free and cost-effective solution for extended vehicle use.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                2. What are the services offered by D2Mech?
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                D2Mech provides services that include long-term car rentals and car repair & maintenance.
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                3. How much does it cost to rent a car in Ahmedabad?
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                Renting a car in Ahmedabad depends on various factors such as duration, brand and car model. Reach out to us and get a quote.
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-12">

                            <h2>Get an instant quote <br> for car services</h2>
                            <form action="save-info.php" method="POST" id="footer_form">
                                <div class="input-block">
                                    <input type="text" name="name" id="" placeholder="Name">
                                </div>
                                <div class="input-block">
                                    <input type="email" name="email" id="" placeholder="Email ID">
                                </div>
                                <div class="input-block">
                                    <input type="hidden" name="country_code" value="91" id="country_code">
                                    <input type="tel" name="phone" class="phone" id="" placeholder="Phone Number">
                                </div>
                                <div class="input-block">
                                    <input type="text" name="car_modal" id="" placeholder="Enter your car model">
                                </div>
                                <div class="input-block">
                                    <input type="submit" name="submit" id="" value="Check Price">
                                </div>

                                <!-- utm Source -->
                                 <input type="hidden" name="form_name" value="Footer Form">
                                 <input type="hidden" name="utm_source" value="<?php echo $utm_source; ?>">
                                 <input type="hidden" name="utm_medium" value="<?php echo $utm_medium; ?>">
                                 <input type="hidden" name="utm_campaign" value="<?php echo $utm_campaign; ?>">
                                 <input type="hidden" name="utm_content" value="<?php echo $utm_content; ?>">
                                 <input type="hidden" name="pageUrl" value='<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>'>
                                <!-- utm Source -->
                            </form>
                            <div class="">
                                <div class="row">
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12">
                                        <div class="contact-wrapper">
                                            <div class="contact-details">
                                                <h2>Contact Us</h2>
                                                <p><a href="tel:+916355978849" style="text-decoration: none;color: inherit;"><span><i class="fa-solid fa-phone"></i></span> +91 6355978849 </a></p>
                                                <p><a href="mailto:support@d2m.ooo" style="text-decoration: none;color: inherit;"><span><i class="fa-solid fa-envelope"></i></span>support@d2m.ooo</a></p>
                                            </div>
                                            <div class="social-details">
                                                <h2>Follow Us</h2>
                                                <ul>
                                                    <li><a href="https://www.facebook.com/D2MIndia/" target="_blank"><i class="fa-brands fa-facebook-f"></i></a></li>
                                                    <li><a href="https://www.instagram.com/d2mechindia_official/"><i class="fa-brands fa-instagram"></i></a></li>
                                                    <li><a href="https://www.linkedin.com/company/d2mech-solutions/" target="_blank"><i class="fa-brands fa-linkedin-in"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-12">
                            <div class="copyright_box">
                                <p>COPYRIGHT © <?php echo date("Y") ?> - D2M | <a href="https://d2m.ooo/privacy-policy.php" target="_blank">Privacy Policy</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>






        </article>
    </main>

    <div id="ohsnap"></div>

    <!-- scripts works here -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="./assets/js/jquery.min.js"><\/script>');
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script type="rocketlazyloadscript" src="./assets/js/ohsnap.min.js"></script>
    <script type="rocketlazyloadscript" src="//cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script type="rocketlazyloadscript" src="./assets/js/intlTelInput.min.js"></script>
    <script type="rocketlazyloadscript" src="./assets/js/owl.carousel.min.js"></script>
    <script type="rocketlazyloadscript" src="./assets/js/magnific-popup.js"></script>
    <script type="rocketlazyloadscript" src="./assets/js/main.js"></script>


    <script>
        $(document).ready(function() {

            if ($(window).innerWidth() < 990) {
                $("header .btn11, header ul.menu a").on("click", function() {
                    $("header .btn11").toggleClass("open");
                    $("header ul.menu").slideToggle();
                    $("header .menu").toggleClass("active");
                });
            }
            $(".menu li a").on("click", function() {
                $(".menu li a").removeClass("active");
                $(this).addClass("active");
            });
        })
    </script>
</body>

</html>