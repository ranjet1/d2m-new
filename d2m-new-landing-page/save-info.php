<?php

include('config.php');

function saveReference($data, $file)
{
  global $db, $conn;

  $name = $conn->real_escape_string(isset($data['name']) ? filter_var($data['name'], FILTER_SANITIZE_STRING) : false);
  $country_code = $conn->real_escape_string(isset($data['country_code']) ? filter_var($data['country_code'], FILTER_SANITIZE_NUMBER_INT) : '91');
  $phone = $conn->real_escape_string(isset($data['phone']) ? filter_var($data['phone'], FILTER_SANITIZE_NUMBER_INT) : false);
  $phone = preg_replace('~^[0\D]++|\D++~', '', $phone);

  $email = $conn->real_escape_string(isset($data['email']) ? filter_var($data['email'], FILTER_SANITIZE_EMAIL) : false);

  $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

  if (preg_match($regex, strtolower($email))) {
    $data['email'] = $email;
  } else {
    $data['email'] = $email = false;
  }

  $car_modal = $conn->real_escape_string(isset($data['car_modal']) ? filter_var($data['car_modal'], FILTER_SANITIZE_STRING) : '');

  $form_name = $conn->real_escape_string(isset($data['form_name']) ? filter_var($data['form_name'], FILTER_SANITIZE_STRING) : '');


  $pageUrl = $data['pageUrl'] = $conn->real_escape_string(isset($data['pageUrl']) ? filter_var($data['pageUrl'], FILTER_SANITIZE_URL) : '');

  $utm_source = $conn->real_escape_string(isset($data['utm_source']) ? $data['utm_source'] : '');
  $utm_medium = $conn->real_escape_string(isset($data['utm_medium']) ? $data['utm_medium'] : '');
  $utm_campaign = $conn->real_escape_string(isset($data['utm_campaign']) ? $data['utm_campaign'] : '');
  $utm_matchtype = $conn->real_escape_string(isset($data['matchtype']) ? $data['matchtype'] : '');
  $utm_keyword = $conn->real_escape_string(isset($data['keyword']) ? $data['keyword'] : '');
  $utm_device = $conn->real_escape_string(isset($data['device']) ? $data['device'] : '');
  $utm_placement = $conn->real_escape_string(isset($data['placement']) ? $data['placement'] : '');
  $utm_term = $conn->real_escape_string(isset($data['utm_term']) ? $data['utm_term'] : '');
  $utm_content = $conn->real_escape_string(isset($data['utm_content']) ? $data['utm_content'] : '');

  $project = "D2m";

  $ip_address = $data['ip_address'] = get_client_ip();

  if ($email && $phone) {

    $dateTime = $data['date'] = date('Y-m-d H:i:s');

    $data['phone'] = $phone =  '+' . $country_code . $phone;

    $result = mysqli_query($conn, "INSERT INTO " . _TABLE_NAME_ . "(`name`, `country_code`, `phone`, `email`, `car_modal`, `project_name`, `form_name`, `utm_source`, `utm_medium`, `utm_term`, `utm_content`, `utm_campaign`, `page_url`, `ip_address`, `date_time`) VALUES ('$name', '$country_code', '$phone', '$email', '$car_modal', '$project', '$form_name', '$utm_source', '$utm_medium', '$utm_term', '$utm_content', '$utm_campaign', '$pageUrl', '$ip_address', '$dateTime')");

    if ($result)
      $response = showResponse(1, 'Information Successfully Saved.', '');
    else
      $response = showResponse(0, 'Sorry, failed you save your data. Please try again later', 0, array('error' => mysqli_error($conn)));
  } else {
    $response = showResponse(-1, 'data insuffient', 0);
  }
  return $response;
}

function getIP()
{
  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $ip = $_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}


function createLog($request, $response, $data, $leadUniqueId)
{
  $today = date('d_m_Y', time());
  $log_file_name = "./logs/" . $today . "_" . $request . ".log";

  $_data = json_decode($response);

  if (strtolower($response) == 'success') {
    $api_status = 'Success';
  } else {
    $api_status = 'Failure';
  }
  $data = stripslashes(json_encode($data, 128));
  $apidate = date('Y-m-d H:i:s');
  $response_log = $leadUniqueId . ": " . $api_status . " at " . $apidate . ",Request: " . $data . "\n\n Response : " . $response . "\n\n\n";
  $logfile = fopen(dirname(__FILE__) . '/' . $log_file_name, "a+");
  fwrite($logfile, $response_log);
  fclose($logfile);
}

// Function to get the client IP address
function get_client_ip()
{
  $ipaddress = '';
  if (getenv('HTTP_CLIENT_IP'))
    $ipaddress = getenv('HTTP_CLIENT_IP');
  else if (getenv('HTTP_X_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
  else if (getenv('HTTP_X_FORWARDED'))
    $ipaddress = getenv('HTTP_X_FORWARDED');
  else if (getenv('HTTP_FORWARDED_FOR'))
    $ipaddress = getenv('HTTP_FORWARDED_FOR');
  else if (getenv('HTTP_FORWARDED'))
    $ipaddress = getenv('HTTP_FORWARDED');
  else if (getenv('REMOTE_ADDR'))
    $ipaddress = getenv('REMOTE_ADDR');
  else
    $ipaddress = 'UNKNOWN';
  return $ipaddress;
}

function showResponse($response_code, $response_message, $response_data, $other_array = array())
{
  header('Content-Type: application/json');
  $responseArray = array(
    "code" => $response_code,
    "message" => $response_message,
    "value" => $response_data,
  );
  if (!empty($other_array))
    $responseArray['info'] = $other_array;
  $responseArray = array("response" => $responseArray);
  return stripslashes(json_encode($responseArray, 128));
}

echo saveReference($_POST, $_FILES);



?>
 <?php mysqli_close($conn); ?>