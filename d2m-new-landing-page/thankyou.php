<?php // include('config.php'); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>D2MECH | Thank You</title>
  <meta content="" name="description">

  <meta content="" name="keywords">

  <!-- Favicons -->
  <link rel="shortcut icon" href="./assets/images/D2m-logo-fevicon.ico" />
  <link rel="apple-touch-icon" sizes="32x32" href="./assets/images/D2m-logo-fevicon.ico" />
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/images/D2m-logo-fevicon.ico" />
  <link rel="apple-touch-icon" sizes="120x120" href="./assets/images/D2m-logo-fevicon.ico" />
  <link rel="apple-touch-icon" sizes="152x152" href="./assets/images/D2m-logo-fevicon.ico" />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <!-- Vendor CSS Files -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
  <link href="assets/css/style.css" rel="stylesheet">


  <!-- End Google Tag Manager -->
  <style>
    .d2m-bannerContent {
      position: absolute;
      width: 45%;
      right: -70px;
      bottom: 50%;
    }

    .contentHeading h2 {
      font-size: 63px;
      line-height: 53px;
    }

    .contentInfo p {
      padding-left: 35px;
    }

    .contact-details h2,
    .social-details h2 {
      font-size: 23px;
      padding-bottom: 20px;
    }

    .contact-details p {
      font-size: 16px;
      color: #5f5f5f;
      padding-left: 0px;
      padding-bottom: 18px;
    }

    @media (max-width: 650px) {
      section.banner-section {
        height: 66vh;
      }

      .d2m-bannerImg img {
        width: 100%;
        position: absolute;
        bottom: 30px;
        left: 0px;
      }

      .d2m-bannerContent {
        position: relative;
        width: 100%;
        right: 0%;
       top: 60px;
      }

      .contentInfo p {
        padding-left: 0px;
        font-size: 31px;
        padding-top: 29px;
      }

      .contact-wrapper {
        width: 90%;
        margin: 30px auto;
      }

      .social-details {
        margin-top: 18px;
      }

      .contact-details h2,
      .social-details h2 {
        font-size: 26px;
        margin-bottom: 21px;
      }

      .contact-details p {
        font-size: 20px;
      }
    }
  </style>
</head>

<body>
  <header>
    <div class="container">
      <div class="row">
        <div class="d-flex justify-content-between align-items-center header-wrapper">
          <div style="width: 100px;">
            <div class="mobile_logo">
              <img src="./assets/images/D2m-logo.png" alt="" />
            </div>
          </div>
          <div class="d-flex align-items-center justify-content-end d-lg-none position-relative w-100">
            <div class="top_menu_wrapper">
              <a href="tel:01234567890" id="" class="call_us_btn">
                <span class=""><i class="fa-solid fa-phone"></i></span>
              </a>
              <div class="menu btn11" data-menu="11">
                <div class="icon-left"></div>
                <div class="icon-right"></div>
              </div>
            </div>

          </div>

          <div class="right_side_menu">
            <ul class="menu">
              <li>
                <a href="https://stage.odigma.com/d2m-stage/#overview">Overview</a>
              </li>
              <li>
                <a href="https://stage.odigma.com/d2m-stage/#choose">Why Choose?</a>
              </li>
              <li>
                <a href="https://stage.odigma.com/d2m-stage/#our-services">Our Services</a>
              </li>
              <li>
                <a href="https://stage.odigma.com/d2m-stage/#testimonials">Testimonials</a>
              </li>
              <li>
                <a href="https://stage.odigma.com/d2m-stage/#faq">FAQ's</a>
              </li>

            </ul>
          </div>
        </div>
      </div>
    </div>
  </header>
  <section class="banner-section">
    <div class="">
      <div class="row direct">
        <div class="containerBorder">
          <div class="d2m-bannerImg">
            <img src="assets/images/car2.webp" alt="Car Image">
          </div>
        </div>
        <div class="d2m-bannerContent">
          <div class="d2mContent">
            <div class="contentHeading">
              <h2>Thank<span style="color: #42AD4F;"> You!</span></h2>
            </div>
            <div class="contentInfo">
              <p>We will contact you shortly </p>
            </div>
            <!-- <div class="d2m-appLink">
              <div class="d2m-app">
                <a href="#"><img src="assets/images/android-cta.png" alt="Google Play Store"></a>
              </div>
              <div class="d2m-app">
                <a href="#">
                  <img src="assets/images/iphone-cta.png" alt="Apple Store">
                </a>
              </div>
            </div> -->

          </div>

        </div>
      </div>



    </div>
  </section>

  <div class="py-4">
    <div class="container">

      <div class="row">
        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12">
          <div class="contact-wrapper">
            <div class="contact-details">
              <h2>Contact Us</h2>
              <p><span><i class="fa-solid fa-phone"></i></span>+91 6355978849</p>
              <p><span><i class="fa-solid fa-envelope"></i></span>support@d2m.ooo</p>
            </div>
            <div class="social-details">
              <h2>Follow Us</h2>
              <ul>
                <li><a href="https://www.facebook.com/D2MIndia/" target="_blank"><i class="fa-brands fa-facebook-f"></i></a></li>
                <li><a href="https://www.instagram.com/d2mechindia_official/"><i class="fa-brands fa-instagram"></i></a></li>
                <li><a href="https://www.linkedin.com/company/d2mech-solutions/" target="_blank"><i class="fa-brands fa-linkedin-in"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-12">
          <div class="copyright_box">
            <p>COPYRIGHT © <?php echo date("Y") ?> - D2M | <a href="https://d2m.ooo/privacy-policy.php" target="_blank">Privacy Policy</a></p>
          </div>
        </div>
      </div>

    </div>
  </div>










  <!-- Vendor JS Files -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script>
    window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')
  </script>
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
  <script>
    $(document).ready(function() {

      if ($(window).innerWidth() < 650) {
        $("header .btn11, header ul.menu a").on("click", function() {
          $("header .btn11").toggleClass("open");
          $("header ul.menu").slideToggle();
          $("header .menu").toggleClass("active");
        });
      }
      $(".menu li a").on("click", function() {
        $(".menu li a").removeClass("active");
        $(this).addClass("active");
      });
    })
  </script>






</body>

</html>