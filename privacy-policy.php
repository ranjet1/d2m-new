<?php 
    // include 'config.php';

    $utm_source = (isset($_GET['utm_source']) ? $_GET['utm_source'] : '');
    $utm_medium = (isset($_GET['utm_medium']) ? $_GET['utm_medium'] : '');
    $utm_term = (isset($_GET['utm_term']) ? $_GET['utm_term'] : '');
    $utm_content = (isset($_GET['utm_content']) ? $_GET['utm_content'] : '');
    $utm_campaign = (isset($_GET['utm_campaign']) ? $_GET['utm_campaign'] : '');
    $pageUrl = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>D2M | Privacy Policy</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="assets/images/fav-icon.png">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png"/>

    <!-- Stylesheets
    ================================================== -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="assets/css/animate.css"> -->
    <!-- <link rel="stylesheet" href="assets/css/owl.carousel.min.css"> -->
    <!-- <link rel="stylesheet" href="assets/css/magnific-popup.css"> -->
    <!-- <link rel="stylesheet" href="assets/css/ohsnap.css"> -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600&display=swap" rel="stylesheet">
</head>

<body class="page">
<style>
    .privacy_policy_wrapper{

    }
    .privacy_policy_wrapper h1{
        color: black;
    font-style: normal;
    letter-spacing: inherit;
    font-size: 50px;
    font-weight: 600;
    }
    .privacy_policy_wrapper .sub-title{
            font-size: 25px;
    margin-bottom: 8px;
    margin-top: 35px;
    }
    .privacy_policy_wrapper p{
            font-size: 20px;
    margin-bottom: 15px;
    text-align: left;
    }
    .privacy_policy{
            margin-top: 10px;
    }
    footer{
            text-align: center;
    padding: 25px;
    }
    footer p{
    text-align: center;
    font-size: 18px;
        font-weight: 500;
    }
    footer p a{

    }
</style>
    <div class="wrapper">
        <div class="header clearfix">            
            <div class="header-top">
                <div id="brand">
                    <a href="./">
                        <img src="assets/images/logo.png" alt="" title="">
                    </a>
                </div>
                <div class="contact-now">
                    <p>Contact us</p>
                    <a href="tel:9016118369"><img src="assets/images/phone-call.png" alt="">+91 9016118369</a>
                    <a href="mailto:Support@d2m.ooo"><img src="assets/images/email.png" class="email-img" alt="">Support@d2m.ooo</a>
                </div>
            </div>
        </div>
        <div class="content privacy_policy">
            <div class="privacy_policy_wrapper">
                <div class="container">
                    <h1 class="main-title">Privacy Policy</h1>

                    <h3 class="sub-title">INTRODUCTION</h3>
                    <p>Your privacy is paramount important to D2M. This privacy statement provides you with our policies regarding the Data and Privacy protection which are followed by the D2M (hereinafter referred to as “D2M”) with respect to the personal information collected and processed by the D2M of its Clients, Vendors, Contractors etc. (“Data Subjects”). </p>
                    <p>This Privacy statements elucidates our policies regarding collection, use, processing and disclosure of personal data when you use our Service and the choices you have associated with that data.</p>
                    <p>D2M hereinafter in the Privacy Statement referred to as (“us”, “we”, or “our”)</p>
                    <p>Our privacy policy is subject to change at any time without prior notice. To make sure you are aware of any changes, please review this policy periodically.</p>
                    <p>By visiting this website, you agree by the terms and conditions of this Privacy Policy page. By accessing our website, you expressly consent to our use and disclosure of your personal information under this Privacy Policy.</p>
                    <h3 class="sub-title">THE INFORMATION WE COLLECT</h3>
                    <p>When you access our Website, we collect and store your personal information which is provided by you from time to time. We aim to provide you a safe, efficient, and smooth experience. This allows us to provide services and features that most likely meet your needs and also helps us to customize our website to improve the user experience.</p>
                    <p>You can browse our website without revealing your identity or any personal information about yourself. Wherever possible, we have indicated the optional and required fields. You may opt-out of our newsletter and services whenever you decide. We may automatically track certain information about you based upon your behavior on our website. We use this information to conduct internal research on our users’ demographics, interests, and behavior to better understand, protect and serve them. This information is compiled and analyzed on an aggregated basis. This information may include the URL that you just visited (whether this URL is on our Website or not), your computer browser model, and your IP address.</p>
                    <p>We use “cookies” on certain pages of the Website to analyze our web page flow, measure promotional effectiveness, and promote trust and safety. “Cookies” are small identifiers sent from a web server and stored on your computer’s hard drive, which help us to recognize you if you visit our website again.</p>
                    <p>Additionally, you may encounter “cookies” or other similar devices on certain pages of the website that are placed by third parties. We do not control the use of cookies by third parties. We collect information about your behavior if you decide to get services from our website.</p>
                    <p>In case you transact with us, we collect certain additional information, such as a billing address, credit/debit card number, card expiration date, and/or other payment details and tracking information from money orders and cheques.</p>
                    <p>In case you choose to post messages on our chat rooms, message boards, or provide feedback, we will collect that information. We retain this information as necessary to resolve disputes, provide customer support and troubleshoot problems as permitted by law.</p>
                    <p>In case you send us personal correspondence, such as emails or letters, or if other users or third parties send us correspondence about your activities or postings on the website, we may collect such information into a file specific to you.</p>
                    <p>We collect personally identifiable information (email address, name, phone number, credit card/debit card, and other payment instrument details, etc.) when you set up a free account with us.</p>
                    <p>You can browse all the sections of our website without being a registered member. Being an IT enterprise solution provider, we ask you to contact us when you require any services or solutions. We do use this information to send you emails and keep you in the loop for future references and interests.</p>
                    <h3 class="sub-title">USING & SHARING YOUR INFORMATION</h3>
                    <p>We NEITHER share NOR sell your personally-identifiable data without your permission unless outlined in this Privacy Policy. The information we receive from you may be used by us or shared by us with our corporate affiliates, dealers, agents, vendors, and other third parties to help process your requests and orders and to help improve our website or the services that we offer; for research; to better understand our customers’ needs; to develop new offerings; to alert you of the new services which you may find interest in. We may also combine the new information with the one we already have in our database to serve you better.</p>
                    <h3 class="sub-title">COOKIES</h3>
                    <p>From time to time, we may place “cookies” on your personal computer. Also, our site uses cookies to track how you found our website. To protect your privacy, we do not use cookies to store or transmit any personal information about you on the Internet. You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline the cookies, certain features of our website may not function properly or at all as a result.</p>
                    <h3 class="sub-title">LINKS</h3>
                    <p>Our website contains links to other sites. Such other sites may use information about your visit to this website. Our Privacy Policy does not apply to practices of such sites that we do not own or control or to people we do not employ. Therefore, we are not responsible for the privacy practices or the accuracy or the integrity of the content included on such other sites. We encourage you to read the individual privacy statements of such websites.</p>
                    <h3 class="sub-title">SECURITY</h3>
                    <p>We secure your privacy with security standards and procedures and comply with applicable privacy laws. Our website combines industry-approved physical, electronic, and procedural safeguards to ensure that your information is well secured in our database.</p>
                    <p>Sensitive data is decrypted, processed, and immediately re-encrypted or discarded when no longer necessary. We host web services in audited data centers, with restricted access to the data processing servers. Controlled access, recorded and live-monitored video feeds, 24/7 staffed security, and biometrics provided in such data centers ensure that we provide secure hosting.</p>
                    <h3 class="sub-title">OPT-OUT POLICY</h3>
                    <p>You have the opportunity to opt-out of receiving non-essential (promotional, marketing-related) communications. Please email support@d2m.ooo  if you no longer wish to receive any information from us.</p>
                    <h3 class="sub-title">CONSENT</h3>
                    <p>By accessing our website and/or by providing your information, you consent to the collection and use of the information you disclose on the website under this Privacy Policy, including but not limited to your consent for sharing your information as per this privacy policy.</p>
                    <p>If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>
                    <h3 class="sub-title">CHANGES TO THIS PRIVACY POLICY</h3>
                    <p>Our privacy policy is subject to change at any time without notice. We may change our Privacy Policy from time to time. You can go through this policy to be aware of the changes.</p>
                    <h3 class="sub-title">QUESTIONS</h3>
                    <p>If you have any questions about our Privacy Policy, please email your questions to us at support@d2m.ooo</p>

                    <h3 class="sub-title">DATA COLLECTED AND FOR WHAT PURPOSE</h3>
                    <p class="sub-section-heading">Approximate Location <span>(Optional)</span></p>
                    <p>App Functionality, Analytics, Advertising or Marketing, Fraud Prevention, Security and Compliance, Personalisation, Account Management</p>
                    <p class="sub-section-heading">Precise Location <span>(Optional)</span></p>
                    <p>App Functionality, Analytics, Advertising or Marketing, Fraud Prevention, Security and Compliance, Personalisation, Account Management</p>
                    
                </div>
        </div>
        <footer>
            <p>COPYRIGHT © 2022 - D2M | <a href="privacy-policy.php">Privacy Policy</a></p>
        </footer>
        </div> <!-- ./ content -->
    </div> <!-- ./ wrapper -->
    
<div id="ohsnap"></div>
    <!-- scripts works here -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="assets/js/jquery-1.11.1.min.js"><\/script>')
    </script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- <script src="assets/js/magnific-popup.js"></script> -->
    <!-- <script src="assets/js/owl.carousel.min.js"></script> -->
    <!-- <script src="assets/js/ohsnap.min.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> -->

    <!-- <script src="assets/js/main.js"></script> -->
</body>

</html>
